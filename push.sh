#!/bin/bash
echo "-----Server 1----";
ssh -o StrictHostKeyChecking=no ubuntu@10.0.6.84 'docker pull registry.gitlab.com/eka.io/server/plugins:aws';
ssh -o StrictHostKeyChecking=no ubuntu@10.0.6.84 'docker stop plugins || true && docker rm -f plugins || true && docker run -h plugin1 -v /data:/app/cache -d -p 0.0.0.0:9000:9000 --env DOCKER_ENV=aws --env NODE_ENV=aws --name plugins registry.gitlab.com/eka.io/server/plugins:aws';
ssh -o StrictHostKeyChecking=no ubuntu@10.0.6.84 'docker stop plugins2 || true && docker rm -f plugins2 || true && docker run -h plugin2 -v /data:/app/cache -d -p 0.0.0.0:9001:9000 --env DOCKER_ENV=aws --env NODE_ENV=aws --name plugins2 registry.gitlab.com/eka.io/server/plugins:aws';
echo "------Server 1 Ends Here----";
echo "-----Server 2----";
ssh -o StrictHostKeyChecking=no ubuntu@10.0.3.251 'docker pull registry.gitlab.com/eka.io/server/plugins:aws';
ssh -o StrictHostKeyChecking=no ubuntu@10.0.3.251 'docker stop plugins || true && docker rm -f plugins || true && docker run -h plugin3 -v /data:/app/cache -d -p 0.0.0.0:9000:9000 --env DOCKER_ENV=automation --env NODE_ENV=automation --name plugins registry.gitlab.com/eka.io/server/plugins:aws';
ssh -o StrictHostKeyChecking=no ubuntu@10.0.3.251 'docker stop plugins2 || true && docker rm -f plugins2 || true && docker run -h plugin4 -v /data:/app/cache -d -p 0.0.0.0:9001:9000 --env DOCKER_ENV=aws --env NODE_ENV=aws --name plugins2 registry.gitlab.com/eka.io/server/plugins:aws';
echo "------Server 2 Ends Here----";
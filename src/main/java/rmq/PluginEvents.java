package rmq;

import java.util.ArrayList;
import java.util.List;

public interface PluginEvents {

     String VOUCHERAPPROVED = "voucher.approved";

     String VOUCHERSETTLED = "voucher.settled";

     String MULTIINVOICEAPPROVED = "invoice.automated.approved";

     String UTILITYAPPROVED = "utility.approved";

     String MULTIINVOICESETTLED = "invoice.automated.settled";

     String UTILITYPAIDEVENT = "invoice.utility.payment";

     String TRAVELBOOKED = "travel.booked";

     String TRAVELCANCELLED = "travel.cancelled";

     static List<String> allEventTypes(){
          List<String> allEvents = new ArrayList<>();
          allEvents.add(VOUCHERAPPROVED);
          allEvents.add(VOUCHERSETTLED);
          allEvents.add(UTILITYPAIDEVENT);
          allEvents.add(MULTIINVOICESETTLED);
          allEvents.add(TRAVELBOOKED);
          allEvents.add(TRAVELCANCELLED);
          return allEvents;
     }
}

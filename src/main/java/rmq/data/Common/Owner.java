package rmq.data.Common;

import lombok.Data;
import rmq.data.Enums.MetaType;

@Data
public class Owner {


    private MetaType ownerType;
    private String ownerId;
    private String ownerName;

}

package rmq.data.Common;

import lombok.Data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Data
public class Transaction {

    private HashSet<String> files = new HashSet<>();

    private String id;

    private Long date;

    private String service;

    private Long amount;

    private Long claimed;

    private String type;

    private Owner owner;

    private List<Owner> allUsers;

    private HashMap<String,String> customFields;


}

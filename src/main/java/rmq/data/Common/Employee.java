package rmq.data.Common;

import lombok.Data;

import java.util.HashMap;

@Data
public class Employee {

    private String email;

    private String mobile;

    private String name;

    private String code;

    private HashMap<String,String> customFields;
}

package rmq.data.ExpenseVouchers;

import lombok.Data;
import rmq.data.Common.Employee;
import rmq.data.Common.Transaction;

import java.util.ArrayList;
import java.util.List;

@Data
public class ExpenseVoucher {

    private String voucherId;

    private String title;

    private Long voucherDate;

    private Long amount;

    private Long expenseAmount;

    private Long size;

    private Employee employee;

    private List<Transaction> transactions = new ArrayList<>();


}

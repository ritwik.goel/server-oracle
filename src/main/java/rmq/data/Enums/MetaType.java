package rmq.data.Enums;

import io.ebean.annotation.DbEnumValue;

public enum  MetaType {

    EMPLOYEE("EMPLOYEE"),
    VENDOR("VENDOR"),
    TDESK("TDESK"),
    GUEST("GUEST"),
    ADMIN("ADMIN"),
    SUPER("SUPER"),
    COMPANY("COMPANY");

    String dbValue;

    MetaType(String dbValue) {
        this.dbValue = dbValue;
    }

    @DbEnumValue
    public String getValue() {
        return dbValue;
    }
}


package rmq;

import io.vertx.core.json.JsonObject;
import lombok.Data;
import plugins.custom.chaitanya.oracle.models.ChaitanyaOracleInvoiceSync;
import plugins.custom.cjdarcl.sap.models.DarclSapInvoiceSync;
import plugins.custom.cjdarcl.sapclone.models.DarclSapCloneInvoiceSync;

@Data
public class PluginEvent {

    private String code;

    private String event;

    private String resource;

    public PluginEvent() {
    }

    public PluginEvent(DarclSapInvoiceSync darclSapInvoiceSync, JsonObject requestData) {
        this.code = darclSapInvoiceSync.getCode();
        this.event = darclSapInvoiceSync.getEventType();
        this.resource = requestData.toString();
    }

    public PluginEvent(DarclSapCloneInvoiceSync darclSapInvoiceSync, JsonObject requestData) {
        this.code = darclSapInvoiceSync.getCode();
        this.event = darclSapInvoiceSync.getEventType();
        this.resource = requestData.toString();
    }

    public PluginEvent(ChaitanyaOracleInvoiceSync darclSapInvoiceSync, JsonObject requestData) {
        this.code = darclSapInvoiceSync.getCode();
        this.event = darclSapInvoiceSync.getEventType();
        this.resource = requestData.toString();
    }
}

package rmq;

import io.vertx.core.Vertx;
import io.vertx.rabbitmq.RabbitMQClient;
import io.vertx.rabbitmq.RabbitMQConsumer;
import io.vertx.rabbitmq.RabbitMQOptions;
import libs.configFactory.ConfigHelper;
import libs.utils.Mapper;
import plugins.core.BasicPlugin;
import plugins.core.PluginType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Semaphore;

public enum RMQRouter {

    INSTANCE;

    private Semaphore virtualThreads = new Semaphore(8);

    private final String APP_EXCHANGE = "";
    private final String PLUGIN_QUEUE = "plugin-queue";
    private final String MAIN_QUEUE = "main-queue";

    RabbitMQClient client;

    public void createRMQClient(Vertx vertx){
        if (this.client==null){
            RabbitMQOptions config = new RabbitMQOptions();
            config.setHost(ConfigHelper.INSTANCE.getMainConfig().getJsonObject("rmq").getString("url"));
            this.client = RabbitMQClient.create(vertx, config);
            this.client.start(a -> {
                if(a.succeeded()){
                    RMQRouter.INSTANCE.runConsumer(this.client);
                }else {
                    a.cause().printStackTrace();
                }
            });

        }
    }

    private void runConsumer(RabbitMQClient client) {
        System.out.println("Running Consumers");
        runThread(client, PLUGIN_QUEUE, virtualThreads);
    }

    private void runThread(RabbitMQClient client, String plugin_queue, Semaphore virtualThreads) {
        client.queueDeclare(plugin_queue,true,false,false, event -> {
            if(event.succeeded()){
                client.basicConsumer(plugin_queue, rs -> {
                    if (rs.succeeded()) {
                        RabbitMQConsumer mqConsumer = rs.result();
                        handleConsumer(mqConsumer, virtualThreads);
                    } else {
                        rs.cause().printStackTrace();
                    }
                });
            }
        });
    }

    private void handleConsumer(RabbitMQConsumer mqConsumer, Semaphore virtualThreads) {
        mqConsumer.handler(message -> {
            try {
                virtualThreads.acquire();
                CompletableFuture.supplyAsync(() -> {
                    try {
                        PluginEvent pluginEvent = Mapper.INSTANCE.getGson().fromJson(message.body().toString(), PluginEvent.class);
                        System.out.println("-------Received Event-------");
                        System.out.println(pluginEvent.getEvent());
                        System.out.println(pluginEvent.getCode());
                        System.out.println("----------------------------");
                        for (PluginType pluginType : PluginType.values()){
                            BasicPlugin plugin = pluginType.getPlugin();
                            if (plugin!=null &&  plugin.canHandle(pluginEvent.getEvent())){
                                pluginType.getPlugin().handle(pluginEvent);
                            }
                        }
                    } finally {
                        virtualThreads.release();
                    }
                    return true;
                });
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}

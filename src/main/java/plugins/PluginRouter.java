package plugins;

import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.Router;
import libs.utils.SubRouterProtocol;

import java.util.Arrays;

public enum PluginRouter implements SubRouterProtocol {

    INSTANCE;


    @Override
    public Router router(Vertx vertx) {
        Router router = Router.router(vertx);

       return router;
    }

}

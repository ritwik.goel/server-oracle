package verticles;

import database.eventData.IntegrationSync;
import io.vertx.core.Vertx;
import libs.configFactory.ConfigHelper;
import plugins.core.BasicPlugin;
import plugins.core.CronJobs;
import plugins.core.PluginType;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public enum Crons {

    INSTANCE;

    private SimpleDateFormat dateFormat;

    public SimpleDateFormat getDateFormat() {
        if (dateFormat == null) {
            dateFormat = new SimpleDateFormat("HH:mm", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return dateFormat;
    }

    public void handle(Vertx vertx) {
        if (ConfigHelper.INSTANCE.isAutomation())
            vertx.setPeriodic(60 * 1000, event -> {
                String time = getDateFormat().format(new Date());
                IntegrationSync.INSTANCE.runIntegrationSync(time);
                for (PluginType value : PluginType.values()) {
                    BasicPlugin plugin = value.getPlugin();
                    if (plugin != null) {
                        CronJobs cronJobs = plugin.cron();
                        if (cronJobs != null) {
                            if (cronJobs.canRun(time)) {
                                cronJobs.run(event);
                            }
                        }
                    }
                }
            });
    }

}

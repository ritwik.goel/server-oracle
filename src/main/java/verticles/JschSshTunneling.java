package verticles;

import com.jcraft.jsch.*;
import libs.utils.RegexFormulas;

import java.io.File;
import java.nio.file.Files;
import java.util.*;

public class JschSshTunneling {

    synchronized public void connectionScriptV2SshTunnel() throws Exception {
        System.out.println("Ecom SFTP Invoice Rejection File download started");

        String sshPrivateKey = "./build/resources/main/seri";
        JSch jsch = new JSch();
        jsch.addIdentity(sshPrivateKey);

        StringBuilder outputBuffer = new StringBuilder();

        Session session = jsch.getSession("opc", "141.148.214.185", 22);
        localUserInfo lui = new localUserInfo();
        session.setUserInfo(lui);
        session.setConfig("StrictHostKeyChecking", "no");
        System.out.println("Creating connection to server: 141.148.214.185");
        session.connect();
        // create port from 2233 on local system to port 22 on tunnelRemoteHost
        System.out.println("Connected to server, initializing Port forwarding ");
        session.setPortForwardingR(10800, "127.0.0.1", 10800);
        System.out.println("Port forward successful forwarding: localhost: 10800 -> 10800:10800");
        session.openChannel("direct-tcpip");

        ProxySOCKS5 proxy = new ProxySOCKS5("127.0.0.1", 10800);
        // create a session connected to port 2233 on the local host.
        Session secondSession = jsch.getSession("seri", "172.18.0.120", 22);
        secondSession.setPassword("Ecom#123");
        secondSession.setUserInfo(new localUserInfoSftp());
        secondSession.setConfig("StrictHostKeyChecking", "no");
        secondSession.setProxy(proxy);
        secondSession.connect();
        ChannelSftp channelSftp = (ChannelSftp) secondSession.openChannel("sftp");
        channelSftp.connect();
        channelSftp.cd("/integration/SERI/Invoice-Creation");
        String fileName = "02_06_2022.csv";

        Vector fileList = channelSftp.ls("/integration/SERI/Invoice-Creation");
        for (int i = 0; i < fileList.size(); i++) {
            ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) fileList.get(i);
            System.out.println(entry.getFilename());
            System.out.println("fileName :: " + fileName);
            if (entry.getFilename().contains(fileName)) {
                try {
                    File downloadedFile = File.createTempFile(UUID.randomUUID().toString(), ".csv");
                    channelSftp.get(entry.getFilename(), downloadedFile.getAbsolutePath());
                    String fileText = new String(Files.readAllBytes(downloadedFile.getAbsoluteFile().toPath()));
                    System.out.println("downloadPath :: " + entry.getFilename());
                    System.out.print("fileText :: " + fileText);

                    List<String> csvAsList = Arrays.asList(fileText.split("\\\n"));
                    String[] keysArr = csvAsList.get(0).split("\\|");
                    System.out.println("keysArr :: " + Arrays.deepToString(keysArr));
                    for (int index = 1; index < csvAsList.size(); index++) {
                        try {
                            String item = csvAsList.get(index);
                            System.out.println("item :: " + item);
                            System.out.println("index :: " + index);
                            String[] values = item.split(RegexFormulas.PSVDELIMETERWITHCOMMA);
                            HashMap<String, String> employee = new HashMap<>();
                            for (int j = 0; j < keysArr.length; j++) {
                                String key = keysArr[j];
                                employee.put(key.trim(), values[j].trim());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        }

        if (secondSession != null && secondSession.isConnected()) {
            System.out.println("Closing SSH Connection");
            secondSession.disconnect();
        }
        if (session != null && session.isConnected()) {
            System.out.println("Closing SSH Connection");
            session.disconnect();
        }

        System.out.print(outputBuffer.toString());
        channelSftp.exit();
        System.out.println("Ecom SFTP Invoice Rejection File download ended");
    }

    public static void connectionScriptV1() throws Exception {
        System.out.println("Biba HRMS SFTP File download started");
        String host = "172.18.0.120";
        int port = 22;
        String user = "seri";
        String pass = "Ecom#123";

        JSch jsch = new JSch();
        Session jschSession = jsch.getSession(user, host, port);
        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        jschSession.setConfig(config);
        jschSession.setPassword(pass);
        ProxySOCKS5 proxy = new ProxySOCKS5("127.0.0.1", 10800);
        jschSession.setProxy(proxy);
        jschSession.connect();
        ChannelSftp channelSftp = (ChannelSftp) jschSession.openChannel("sftp");
        channelSftp.connect();
        channelSftp.cd("/integration/SERI/Invoice-Creation");
        String fileName = "02_06_2022.csv";

        Vector fileList = channelSftp.ls("/integration/SERI/Invoice-Creation");
        for (int i = 0; i < fileList.size(); i++) {
            ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) fileList.get(i);
            System.out.println(entry.getFilename());
            System.out.println("fileName :: " + fileName);
            if (entry.getFilename().contains(fileName)) {
                try {
                    File downloadedFile = File.createTempFile(UUID.randomUUID().toString(), ".csv");
                    channelSftp.get(entry.getFilename(), downloadedFile.getAbsolutePath());
                    String fileText = new String(Files.readAllBytes(downloadedFile.getAbsoluteFile().toPath()));
                    System.out.println("downloadPath :: " + entry.getFilename());
                    System.out.print("fileText :: " + fileText);

                    List<String> csvAsList = Arrays.asList(fileText.split("\\\n"));
                    String[] keysArr = csvAsList.get(0).split("\\|");
                    System.out.println("keysArr :: " + Arrays.deepToString(keysArr));
                    for (int index = 1; index < csvAsList.size(); index++) {
                        try {
                            String item = csvAsList.get(index);
                            System.out.println("item :: " + item);
                            System.out.println("index :: " + index);
                            String[] values = item.split(RegexFormulas.PSVDELIMETERWITHCOMMA);
                            HashMap<String, String> employee = new HashMap<>();
                            for (int j = 0; j < keysArr.length; j++) {
                                String key = keysArr[j];
                                employee.put(key.trim(), values[j].trim());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        }

        channelSftp.exit();
        System.out.println("Biba HRMS SFTP File download ended");
    }

    class localUserInfo implements UserInfo {
        String passwd;

        public String getPassword() {
            return passwd;
        }

        public boolean promptYesNo(String str) {
            return true;
        }

        public String getPassphrase() {
            return null;
        }

        public boolean promptPassphrase(String message) {
            return true;
        }

        public boolean promptPassword(String message) {
            return false;
        }

        public void showMessage(String message) {
        }
    }

    class localUserInfoSftp implements UserInfo {
        String passwd;

        public String getPassword() {
            return passwd;
        }

        public boolean promptYesNo(String str) {
            return true;
        }

        public String getPassphrase() {
            return null;
        }

        public boolean promptPassphrase(String message) {
            return true;
        }

        public boolean promptPassword(String message) {
            return true;
        }

        public void showMessage(String message) {
        }
    }
}

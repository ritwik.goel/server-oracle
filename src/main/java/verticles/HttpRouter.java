package verticles;

import controllers.ApplicationRouter;
import io.vertx.core.http.HttpMethod;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.ext.web.Router;
import io.vertx.rxjava.ext.web.handler.BodyHandler;
import libs.configFactory.ConfigManager;
import org.apache.commons.lang3.time.DateUtils;
import plugins.PluginRouter;
import webhooks.WebhookRouter;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

public class HttpRouter extends AbstractVerticle {


    @Override
    public void start() throws Exception {
        super.start();
        Router router = Router.router(vertx);
        router.route().handler(io.vertx.rxjava.ext.web.handler.CorsHandler.create(".*.")
                .allowedMethod(io.vertx.core.http.HttpMethod.GET)
                .allowedMethod(io.vertx.core.http.HttpMethod.POST)
                .allowedMethod(io.vertx.core.http.HttpMethod.PATCH)
                .allowedMethod(io.vertx.core.http.HttpMethod.PUT)
                .allowedMethod(io.vertx.core.http.HttpMethod.DELETE)
                .allowedMethod(io.vertx.core.http.HttpMethod.OPTIONS)
                .allowedHeader("Access-Control-Request-Method")
                .allowedHeader("Access-Control-Allow-Credentials")
                .allowedHeader("Access-Control-Allow-Origin")
                .allowedHeader("Access-Control-Allow-Headers")
                .allowedHeader("Authorization")
                .allowedHeader("client-id")
                .allowedHeader("client-secret")
                .allowedHeader("Content-Type"));
        router.route("/*").handler(event -> {
            event.put("requestId", UUID.randomUUID().toString());
            if (event.request().method().equals(HttpMethod.GET) || event.request().method().equals(HttpMethod.DELETE)) {
                event.next();
            } else if (event.request().getHeader("content-type") != null && event.request().getHeader("content-type").toLowerCase().contains("application/json")) {
                BodyHandler.create().handle(event);
            } else if (event.request().getHeader("Content-Type") != null && event.request().getHeader("Content-Type").toLowerCase().contains("application/json")) {
                BodyHandler.create().handle(event);
            } else {
                event.next();
            }
        });

        router.get("/health").handler(event -> {
            event.response().end("ok :: "+new Timestamp(DateUtils.addDays(new Date(),-1).getTime()).toString());
        });

        router.get("/ecomSshD").handler(event -> {

            AmazonTest.INSTANCE.dynamicSshRun();

            event.response().end("Done :: "+new Timestamp(DateUtils.addDays(new Date(),-1).getTime()).toString());
        });

        router.get("/test").handler(event -> {
            StringBuffer stringBuffer = new StringBuffer();

            try {
                stringBuffer.append(String.format("maximum amount of memory that the virtual machine :: %s MB\n",String.valueOf(Runtime.getRuntime().maxMemory() / (1024 * 1024))));
                stringBuffer.append(String.format("approximation to the total amount of memory currently available :: %s MB\n",String.valueOf(Runtime.getRuntime().freeMemory() / (1024 * 1024))));
                stringBuffer.append(String.format("total amount of memory currently available for current :: %s MB\n",String.valueOf(Runtime.getRuntime().totalMemory() / (1024 * 1024))));
                stringBuffer.append(String.format("total number of processors :: %s\n",String.valueOf(Runtime.getRuntime().availableProcessors())));
                System.out.println(Runtime.getRuntime().maxMemory() / (1024 * 1024));
                System.out.println(Runtime.getRuntime().availableProcessors());
                System.out.println(Runtime.getRuntime().freeMemory() / (1024 * 1024));
                System.out.println(Runtime.getRuntime().totalMemory() / (1024 * 1024));
            }catch (Exception e){
                e.printStackTrace();
            }
            try {
                InetAddress loopBackAdd = InetAddress.getLoopbackAddress();
                System.out.println("loopBackAdd name :: "+loopBackAdd.getHostName());
                System.out.println("loopBackAdd host address :: "+loopBackAdd.getHostAddress());
                System.out.println("loopBackAdd conical hostname :: "+loopBackAdd.getCanonicalHostName());
                stringBuffer.append(String.format("loopBackAdd name :: %s\n",loopBackAdd.getHostName()));
                stringBuffer.append(String.format("loopBackAdd host address :: %s\n",loopBackAdd.getHostAddress()));
                stringBuffer.append(String.format("loopBackAdd conical hostname :: %s\n",loopBackAdd.getCanonicalHostName()));
                InetAddress localHost = InetAddress.getLocalHost();
                System.out.println("host name :: "+localHost.getHostName());
                System.out.println("host host address :: "+localHost.getHostAddress());
                System.out.println("host conical hostname :: "+localHost.getCanonicalHostName());
                stringBuffer.append(String.format("localHost name :: %s\n",localHost.getHostName()));
                stringBuffer.append(String.format("localHost host address :: %s\n",localHost.getHostAddress()));
                stringBuffer.append(String.format("localHost conical hostname :: %s\n",localHost.getCanonicalHostName()));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            event.response().end(stringBuffer.toString());
        });

        router.mountSubRouter("/eka", ApplicationRouter.INSTANCE.router(vertx));
        router.mountSubRouter("/webhook", WebhookRouter.INSTANCE.router(vertx));
        router.mountSubRouter("/plugins", PluginRouter.INSTANCE.router(vertx));
        vertx.createHttpServer().requestHandler(router::accept).listen(ConfigManager.INSTANCE.getMainConfig().getInteger("httpRouterPort"));
    }

}

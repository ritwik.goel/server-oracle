package verticles;

import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.config.hosts.HostConfigEntry;
import org.apache.sshd.client.config.hosts.HostConfigEntryResolver;
import org.apache.sshd.client.config.keys.ClientIdentityLoader;
import org.apache.sshd.client.keyverifier.AcceptAllServerKeyVerifier;
import org.apache.sshd.client.session.ClientSession;
import org.apache.sshd.common.config.keys.loader.KeyPairResourceLoader;
import org.apache.sshd.common.forward.PortForwardingEventListener;
import org.apache.sshd.common.keyprovider.KeyIdentityProvider;
import org.apache.sshd.common.keyprovider.KeyPairProvider;
import org.apache.sshd.common.session.Session;
import org.apache.sshd.common.util.net.SshdSocketAddress;
import org.apache.sshd.common.util.security.SecurityUtils;
import org.apache.sshd.server.forward.AcceptAllForwardingFilter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Collection;

public enum AmazonTest {

    INSTANCE;

    public static PrivateKey getPemPrivateKey(String filename) throws Exception {

        String privateKeyContent = new String(Files.readAllBytes(Paths.get(filename)));
        privateKeyContent = privateKeyContent.replaceAll("\\n", "").replace("-----BEGIN RSA PRIVATE KEY-----", "").replace("-----END RSA PRIVATE KEY-----", "");
        KeyFactory kf = KeyFactory.getInstance("RSA");

        PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));

        PrivateKey privKey = kf.generatePrivate(keySpecPKCS8);
        return privKey;
    }

    public static PublicKey getPemPublicKeyFromPrivate(PrivateKey privateKey) throws Exception {

        RSAPrivateCrtKey privk = (RSAPrivateCrtKey)privateKey;

        RSAPublicKeySpec publicKeySpec = new java.security.spec.RSAPublicKeySpec(privk.getModulus(), privk.getPublicExponent());

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey myPublicKey = keyFactory.generatePublic(publicKeySpec);
        return myPublicKey;
    }

    public static PublicKey getPemPublicKey(String filename) throws Exception {

        String publicKeyContent = new String(Files.readAllBytes(Paths.get(filename)));
        publicKeyContent = publicKeyContent.replaceAll("\\n", "").replace("-----BEGIN RSA PRIVATE KEY-----", "").replace("-----END RSA PRIVATE KEY-----", "");
        KeyFactory kf = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyContent));

        RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(keySpecX509);
        return pubKey;
    }

    public static PrivateKey getPrivateKey(String filename) throws Exception {

        byte[] keyBytes = Files.readAllBytes(Paths.get(filename));

        PKCS8EncodedKeySpec spec =
                new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    public static PublicKey getPublicKey(String filename) throws Exception {

        byte[] keyBytes = Files.readAllBytes(Paths.get(filename));

        X509EncodedKeySpec spec =
                new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    synchronized public void dynamicSshRun() {
        try {
            final String BASTION_SERVER_USER = "opc";
            final String BASTION_SEREVR_HOST = "141.148.214.185";

            KeyPairResourceLoader loader = SecurityUtils.getKeyPairResourceParser();
            Collection<KeyPair> keys = loader.loadKeyPairs(null, Paths.get("./build/resources/main/seri"), null);
            // Optional loading keys from a PEM file
//            keys= PEMResourceParserUtils.getPEMResourceParserByAlgorithm("RSA").loadKeyPairs(Paths.get("./build/resources/main/seri"), null);

            // Optional: Using Putty key for login
//            keys=PuttyKeyUtils.DEFAULT_INSTANCE.loadKeyPairs(ClassLoader.getSystemResource("seri").toURI().toURL(), null);

            SshClient client = SshClient.setUpDefaultClient();
            client.setForwardingFilter(AcceptAllForwardingFilter.INSTANCE);
            client.setServerKeyVerifier(AcceptAllServerKeyVerifier.INSTANCE);
            System.out.println("Here1....");
//            client.setKeyIdentityProvider(KeyIdentityProvider.wrapKeyPairs(keys));
            System.out.println("Here2....");
            client.start();
            System.out.println("Here3....");
//            client.addPublicKeyIdentity(keys.iterator().next());
            PrivateKey privateKey = getPemPrivateKey("./build/resources/main/seri");
//            client.addPublicKeyIdentity(new KeyPair(getPemPublicKeyFromPrivate(privateKey), privateKey));
//            client.addPublicKeyIdentity(new KeyPair(null, getPemPrivateKey("./build/resources/main/seri")));
            // using the client for multiple sessions...
            try (ClientSession session = client.connect(BASTION_SERVER_USER, BASTION_SEREVR_HOST, 22).verify()
                    .getSession()) {
                System.out.println("Here4....");
                // IF you use password to login provide here
                // session.addPasswordIdentity(BASTION_SERVER_PASSWORD); // for password-based
                // authentication

//                session.addPublicKeyIdentity(keys.iterator().next());
                session.addPublicKeyIdentity(new KeyPair(getPemPublicKeyFromPrivate(privateKey), privateKey));
//                session.addPublicKeyIdentity(keys);
                // authentication
                // Note: can add BOTH password AND public key identities - depends on the
                // client/server security setup
                session.auth().verify();
                System.out.println("Here5....");
                // start using the session to run commands, do SCP/SFTP, create local/remote
                // port forwarding, etc...
                session.addPortForwardingEventListener(new PortForwardingEventListener() {
                    @Override
                    public void establishedDynamicTunnel(Session session, SshdSocketAddress local,
                                                         SshdSocketAddress boundAddress, Throwable reason) throws IOException {
                        // TODO Auto-generated method stub
                        PortForwardingEventListener.super.establishedDynamicTunnel(session, local, boundAddress, reason);
                        System.out.println("Dynamic Forword Tunnel is Ready");
                    }
                });
                SshdSocketAddress sshdSocketAddress = session
                        .startDynamicPortForwarding(new SshdSocketAddress("127.0.0.1", 10800));
                System.out.println("Host: " + sshdSocketAddress.getHostName());
                System.out.println("Port: " + sshdSocketAddress.getPort());
//                // Create a Proxy object to work with
//                Proxy proxy = new Proxy(Proxy.Type.SOCKS,
//                        new InetSocketAddress(sshdSocketAddress.getHostName(), sshdSocketAddress.getPort()));
                /**
                 * Now you can use this proxy instance into any URL until this SSH session is active.
                 */

                // TEST one URL
//                HttpURLConnection connection = (HttpURLConnection) new URL(URL_TO_ACCESS).openConnection(proxy);
//                System.out.println("Proxy work:" + connection.getURL());
//                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                System.out.println("================== Data From URL ==================\n");
//                String inputLine;
//                while ((inputLine = in.readLine()) != null)
//                    System.out.println(inputLine);
//                in.close();
//                System.out.println("================== Data From URL ==================\n");
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}

package apis;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import plugins.custom.dtdc.ramco.api.FtpsServerApis;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

public enum ApisManager {

    INSTANCE;



    private FtpsServerApis ftpsServerApis;
    public FtpsServerApis getFtpsServerApis(){
        if (ftpsServerApis == null){

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

            Interceptor AUTH_CONTROL_INTERCEPTOR = chain -> {
                Request request = chain.request().newBuilder().addHeader("X-CLIENT-ID", "UH2I3U4G23U4G").addHeader("X-CLIENT-SECRET", "2I3UU2I3UI2YB423YU#HG$12SFE234Y3").build();
                return chain.proceed(request);
            };

            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(
                            new OkHttpClient.Builder()
                                    .addNetworkInterceptor(AUTH_CONTROL_INTERCEPTOR)
                                    .addInterceptor(logging)
                                    .writeTimeout(40,TimeUnit.SECONDS)
                                    .readTimeout(60, TimeUnit.SECONDS)
                                    .build()
                    )
                    .baseUrl("http://10.0.6.84:3001/")
                    .build();
            ftpsServerApis = retrofit.create(FtpsServerApis.class);
        }
        return ftpsServerApis;
    }

}

package controllers;

import controllers.auth.AuthRouter;
import controllers.implementation.EventImplementationImpl;
import controllers.procure.ProcureRouter;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.Router;
import libs.utils.SubRouterProtocol;

public enum ApplicationRouter implements SubRouterProtocol {

    INSTANCE;

    @Override
    public Router router(Vertx vertx) {
        Router router = Router.router(vertx);
        router.mountSubRouter("/user", AuthRouter.INSTANCE.router(vertx));
        router.post("/event/automation.digest").handler(EventImplementationImpl.INSTANCE::handle);

        router.mountSubRouter("/procure", ProcureRouter.INSTANCE.router(vertx));

        return router;
    }
}
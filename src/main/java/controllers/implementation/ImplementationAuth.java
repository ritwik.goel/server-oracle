package controllers.implementation;

import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.ext.web.RoutingContext;
import libs.configFactory.ConfigHelper;
import libs.customErrors.RoutingError;
import libs.utils.ResponseHelper;

public enum ImplementationAuth {

    INSTANCE;

    private JsonObject config = ConfigHelper.INSTANCE.getApplicationConfig();

    public RoutingContext auth(RoutingContext routingContext){
        try {
            String clientId = routingContext.request().getHeader("client-id");
            String clientSecret = routingContext.request().getHeader("client-secret");
            if (clientId == null || !clientId.equals(config.getString("id"))){
                throw new RoutingError(403, "Invalid Client Id.");
            }
            if (clientSecret == null || !clientSecret.equals(config.getString("secret"))){
                throw new RoutingError(403, "Invalid Client Secret.");
            }
            routingContext.next();
        }catch (Exception e){
            e.printStackTrace();
            ResponseHelper.INSTANCE.handleError(routingContext, e);
        }
        return routingContext;
    }
}

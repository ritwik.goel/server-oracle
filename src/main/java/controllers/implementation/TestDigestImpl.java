package controllers.implementation;

import controllers.implementation.utils.Types;
import libs.customErrors.RoutingError;

public enum TestDigestImpl implements DigestIntf {

    INSTANCE;

    @Override
    public String id() {
        return "test.digest";
    }

    @Override
    public String code() {
        return "EKA";
    }

    @Override
    public DigestResponse handle(DigestRequest request) {
        if(!request.getCode().equalsIgnoreCase(code())){
            throw new RoutingError("::Invalid Company Mapped With Trigger::");
        }
        if(request.getType().equals(Types.VENDOR_INVOICE)){
            return handleProcessing(request);
        }
        throw new RoutingError("Unable to handle this type");
    }

    private DigestResponse handleProcessing(DigestRequest request){
        if (true){
            throw new RoutingError("Error found for TEST.");
        }
        return new DigestResponse(false);
    }
}
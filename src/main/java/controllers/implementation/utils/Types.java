package controllers.implementation.utils;

public interface Types {

    final String APPSUBMISSION  = "app-submission";
    final String SURVEYSUBMISSION  = "survey-submission";
    final String VENDOR_INVOICE = "vendor-invoice";
    final String PURCHASE_ORDER = "purchase-order";
    final String VENDOR_INVOICE_SUBMITTED = "invoice-submitted";
    final String VENDOR = "vendor";
    final String UTILITY = "utility";
}

package controllers.implementation;

public interface DigestIntf {

    String id();

    DigestResponse handle(DigestRequest request);

    String code();

}

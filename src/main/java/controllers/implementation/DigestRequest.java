package controllers.implementation;

import lombok.Data;

@Data
public class DigestRequest {

    private String code;

    private String id;

    private String type;

    private String body;

}

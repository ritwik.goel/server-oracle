package controllers.implementation;

import io.vertx.rxjava.core.RxHelper;
import io.vertx.rxjava.ext.web.RoutingContext;
import libs.ParamsController;
import libs.customErrors.RoutingError;
import libs.utils.*;
import plugins.custom.cjdarcl.sap.app.DarclInvoiceDigestImpl;
import plugins.custom.cjdarcl.sap.app.DarclUtilityDigestImpl;
import plugins.custom.cjdarcl.sapclone.app.CloneDarclInvoiceDigestImpl;
import plugins.custom.cjdarcl.sapclone.app.CloneDarclUtilityDigestImpl;
import plugins.quickbooks.app.QuickbooksInvoiceDigestImpl;
import plugins.quickbooks.app.QuickbooksUtilityDigestImpl;
import rx.Single;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public enum EventImplementationImpl implements ParamsController {

    INSTANCE;

    @Override
    public List<RequestItem> items() {
        List<RequestItem> items = new ArrayList<>();
        items.add(RequestItem.builder().key("id").itemType(RequestItemType.STRING).required(true).error("Task Id is required").build());
        items.add(RequestItem.builder().key("code").itemType(RequestItemType.STRING).required(true).error("Company Code is required").build());
        items.add(RequestItem.builder().key("type").itemType(RequestItemType.STRING).required(true).error("Event Type is required").build());
        items.add(RequestItem.builder().key("body").itemType(RequestItemType.STRING).required(true).error("Request body is required").build());
        return items;
    }

    private HashMap<String, DigestIntf> map = new HashMap<>();
    {
        map.put(DarclInvoiceDigestImpl.INSTANCE.id(), DarclInvoiceDigestImpl.INSTANCE);
        map.put(TestDigestImpl.INSTANCE.id(), TestDigestImpl.INSTANCE);
        map.put(DarclUtilityDigestImpl.INSTANCE.id(), DarclUtilityDigestImpl.INSTANCE);
        map.put(CloneDarclInvoiceDigestImpl.INSTANCE.id(), CloneDarclInvoiceDigestImpl.INSTANCE);
        map.put(CloneDarclUtilityDigestImpl.INSTANCE.id(), CloneDarclUtilityDigestImpl.INSTANCE);
        map.put(QuickbooksInvoiceDigestImpl.INSTANCE.id(), QuickbooksInvoiceDigestImpl.INSTANCE);
        map.put(QuickbooksUtilityDigestImpl.INSTANCE.id(), QuickbooksUtilityDigestImpl.INSTANCE);
    }

    @Override
    public void handle(RoutingContext routingContext) {
        Single.just(routingContext)
                .subscribeOn(RxHelper.blockingScheduler(routingContext.vertx()))
                .map(ImplementationAuth.INSTANCE::auth)
                .map(this::map)
                .map(this::doNext)
                .subscribe(response -> ResponseHelper.INSTANCE.writeJsonResponse(routingContext, response), throwable -> {
                    ResponseHelper.INSTANCE.handleError(routingContext, throwable);
                });
    }

    private DigestResponse doNext(RequestZipped eventRq) {
        Request event = eventRq.getRequest();
        DigestRequest request = new DigestRequest();
        request.setBody(event.get("body"));
        request.setCode(event.get("code"));
        request.setId(event.get("id"));
        request.setType(event.get("type"));
        if(map.containsKey(request.getId())){
            return map.get(request.getId()).handle(request);
        }
        throw new RoutingError("Unable to process this request");
    }
}
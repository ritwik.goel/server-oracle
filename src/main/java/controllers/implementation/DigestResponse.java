package controllers.implementation;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@Data
@NoArgsConstructor
public class DigestResponse {

    private boolean success = true;

    private HashMap<String, String> data = new HashMap<>();

    private HashMap<String, String> attrs = new HashMap<>();

    public DigestResponse(boolean success) {
        this.success = success;
    }
}

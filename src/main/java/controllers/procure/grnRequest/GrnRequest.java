package controllers.procure.grnRequest;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class GrnRequest {
    private Long addressId;
    private Long grnDate;
    private String grnId;
    private Map<String, String> extra = new HashMap<>();
    private List<Item> items = new ArrayList<>();
}

package controllers.procure.grnRequest;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class Item {
    private String code;
    private Double qty;
    private Double price;
    private Map<String, String> extra = new HashMap<>();
}

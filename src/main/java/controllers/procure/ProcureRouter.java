package controllers.procure;

import controllers.implementation.ImplementationAuth;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.Router;
import libs.utils.SubRouterProtocol;
import plugins.buildrun.app.grn.GrnRouter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public enum ProcureRouter implements SubRouterProtocol {

    INSTANCE;

    private List<SubRouterProtocol> GRN_CONTROLLERS = new ArrayList<>();
    {
        GRN_CONTROLLERS.add(GrnRouter.INSTANCE);
    }

    @Override
    public Router router(Vertx vertx) {
        Router router = Router.router(vertx);
        /**
         * APIs to be utilized by DICE tool
         */
        router.route("/*").handler(ImplementationAuth.INSTANCE::auth);

        CompletableFuture.supplyAsync(()->{
            for (SubRouterProtocol grn_controller : GRN_CONTROLLERS) {
                router.mountSubRouter("/grn", grn_controller.router(vertx));
            }
           return null;
        });
        return router;
    }
}
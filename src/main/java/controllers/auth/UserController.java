package controllers.auth;

import apis.ApisManager;
import apis.eka.Request.AuthenticateRequest.AuthenticationRequest;
import apis.eka.Response.AuthResponse.AuthenticationResponse;
import apis.eka.Response.ErrorResponse.ErrorResponse;
import database.configs.BaseConfig;
import database.json.PluginObject;
import database.models.Plugins;
import database.repos.PluginRepository;
import io.vertx.rxjava.core.RxHelper;
import io.vertx.rxjava.ext.web.RoutingContext;
import libs.ParamsController;
import libs.configFactory.ConfigManager;
import libs.customErrors.RoutingError;
import libs.utils.Mapper;
import libs.utils.RequestItem;
import libs.utils.ResponseHelper;
import lombok.Data;
import plugins.core.BasicPlugin;
import plugins.core.PluginType;
import retrofit2.Call;
import rx.Single;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public enum UserController implements ParamsController {

    INSTANCE;

    @Override
    public List<RequestItem> items() {
        return new ArrayList<>();
    }


    @Override
    public void handle(RoutingContext routingContext) {
        Single.just(routingContext)
                .subscribeOn(RxHelper.blockingScheduler(routingContext.vertx()))
                .map(this::doNext)
                .subscribe(response -> ResponseHelper.INSTANCE.writeJsonResponse(routingContext, response), throwable -> {
                    ResponseHelper.INSTANCE.handleError(routingContext, throwable);
                });

    }

    private Response doNext(RoutingContext routingContext) {
        Response response = new Response();
        String header = routingContext.request().getHeader("Authorization");
        if (header == null)
            throw new RoutingError("Invalid header");
        AuthenticationRequest request = new AuthenticationRequest();
        request.setToken(header);
        Call<AuthenticationResponse> responseCall = ApisManager.INSTANCE.getEkaAPis().authenticateUser(request);
        try {
            retrofit2.Response<AuthenticationResponse> mR = responseCall.execute();
            if (mR.isSuccessful()) {
                AuthenticationResponse authenticationResponse = mR.body();
                if (authenticationResponse == null)
                    throw new RoutingError("No Company Details found");
                String companyCode = authenticationResponse.getCode();
                if (companyCode == null || companyCode.equalsIgnoreCase(""))
                    throw new RoutingError("No Company Details found");
                List<Plugins> plugins = PluginRepository.INSTANCE.pluginsForCode(companyCode);
                if (plugins.size()==0) {
                    response.setPlugins(
                            Arrays.stream(PluginType.values())
                                    .map(PluginType::getPlugin)
                                    .filter(Objects::nonNull)
                                    .filter(plugin -> {
                                        if(!ConfigManager.INSTANCE.isStaging()){
                                            return !plugin.onDevelopmentMode();
                                        }
                                        return true;
                                    })
                                    .filter(basicPlugin -> {
                                        return basicPlugin.isAvailable(companyCode);
                                    })
                                    .map(BasicPlugin::getObject)
                                    .filter(Objects::nonNull)
                                    .collect(Collectors.toList())
                    );
                    return response;
                } else {
                    HashMap<PluginType,Plugins> pluginsHashMap = new HashMap<>();
                    for (Plugins plugin : plugins) {
                        pluginsHashMap.put(plugin.getPluginType(),plugin);
                    }
                    Arrays.stream(PluginType.values())
                            .map(PluginType::getPlugin)
                            .filter(Objects::nonNull)
                            .filter(plugin -> {
                                if(!ConfigManager.INSTANCE.isStaging()){
                                    return !plugin.onDevelopmentMode();
                                }
                                return true;
                            })
                            .filter(basicPlugin -> {
                                return basicPlugin.isAvailable(companyCode);
                            })
                            .map(BasicPlugin::getObject)
                            .filter(Objects::nonNull)
                            .forEach(plugin -> {
                                if (pluginsHashMap.containsKey(plugin.getPluginType())) {
                                    Plugins userPlugin= pluginsHashMap.get(plugin.getPluginType());
                                    BaseConfig config = userPlugin.getConfig();
                                    PluginObject clone = new PluginObject(plugin.getName(), plugin.getDescription(), plugin.getLogo(), plugin.getPluginType(), plugin.getPath());
                                    clone.setInstalled(true);
                                    clone.setStatus(userPlugin.getStatus().name());
                                    clone.setInstalledOn(config.getInstalledOn());
                                    response.plugins.add(clone);
                                } else {
                                    response.plugins.add(plugin);
                                }
                            });
                    return response;
                }
            } else {
                ErrorResponse error = Mapper.INSTANCE.getGson().fromJson(mR.errorBody().string(), ErrorResponse.class);
                throw new RoutingError(error.getMessage());
            }
        } catch (IOException e) {
            throw new RoutingError(e.getMessage());
        }
    }

    @Data
    class Response {
        private List<PluginObject> plugins = new ArrayList<>();
    }
}

package controllers.auth;

import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.Router;
import libs.utils.SubRouterProtocol;

public enum AuthRouter implements SubRouterProtocol {

    INSTANCE;

    @Override
    public Router router(Vertx vertx) {
        Router router = Router.router(vertx);
        router.get("/").handler(UserController.INSTANCE::handle);
        return router;
    }
}

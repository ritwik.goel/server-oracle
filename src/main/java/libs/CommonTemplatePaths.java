package libs;

public interface CommonTemplatePaths {

    public interface PLUGINS{
        public interface S3BUCKETVOUCHER{
            public String expenseVoucherTemplatePath = "template/s3bucketVoucher/ExpenseVoucher.twig";
        }

        public interface DTDCRAMCO{
            public String newEmployeesReportFinance = "template/dtdcRamco/NewEmployeesReportFinance.twig";
        }

        public interface BLACKBUCKREPORTS{
            public String weeklyReimbursementReport = "template/reports/blackbuck/reimbursementReport.twig";
        }
    }

    public interface REPORTS{
        public interface COMMON{
            public String exportWizardUnbounded="template/reports/common/exportWizardUnbounded.twig";
        }

        public interface DTDC{
            public String financeSettlements="template/reports/dtdc/financeSettlements.twig";
            public String financeWiseReport="template/reports/dtdc/financeWiseReports.twig";
            public String advanceReport="template/reports/dtdc/advanceReports.twig";
        }
    }

    public interface APPROVAL{
        public String sendApproval = "template/sendApproval.twig";
        public String sendCustomApproval = "template/sendCustomApproval.twig";
        public String afterApproval = "template/afterApproval.twig";
        public String confirmationMail = "template/confirmationMail.twig";
        public String onSubmitConfirmationMail = "template/onSubmitConfirmationMail.twig";
        public String pendingAlertMail = "template/pendingAlertMail.twig";
    }
}

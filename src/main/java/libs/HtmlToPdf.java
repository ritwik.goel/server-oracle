package libs;

import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import libs.configFactory.ConfigManager;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

public class HtmlToPdf {

    public static final String DESTPATH = ConfigManager.INSTANCE.getMainConfig().getString("pdfpath");


    public static String convertHtmlToPdf(String html, String service) {
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();
        String filePath = DESTPATH + service + "/" + UUID.randomUUID().toString() + ".pdf";
        String directoryName = DESTPATH + service;
        File directory = new File(String.valueOf(directoryName));
        try {
            if (!directory.exists()) {
                directory.mkdirs();
            }
            FileOutputStream outputStream = new FileOutputStream(filePath);
            renderer.createPDF(outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }

    public static String convertUrlToPdf(String url, String service) {
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocument(url);
        renderer.layout();
        String filePath = DESTPATH + service + "/" + UUID.randomUUID().toString() + ".pdf";
        String directoryName = DESTPATH + service;
        File directory = new File(String.valueOf(directoryName));
        try {
            if (!directory.exists()) {
                directory.mkdirs();
            }
            FileOutputStream outputStream = new FileOutputStream(filePath);
            renderer.createPDF(outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }

    public static String addImageToPdf(String src, String imageUrl, String imageName, int pageNum) {
        String destPath = src.substring(0,src.lastIndexOf("/")+1) + UUID.randomUUID()+".pdf";
        try {
            PdfReader reader = new PdfReader(src);
            PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(destPath));
            Image image = Image.getInstance(imageUrl);
            PdfImage stream = new PdfImage(image, imageName+pageNum, null);
            stream.put(new PdfName("ITXT_SpecialId"), new PdfName(imageName+pageNum));
            PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
            image.setDirectReference(ref.getIndirectReference());
            image.setAbsolutePosition(50, 100);
            image.scaleAbsolute((image.getWidthPercentage()*500)/100,800);
            stamper.insertPage(pageNum, new Rectangle(595,900));
            PdfContentByte over = stamper.getOverContent(pageNum);
            over.addImage(image);
            stamper.close();
            reader.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            new File(src).delete();
        }
        return destPath;
    }
}
package libs.customErrors;



public class RoutingError extends RuntimeException {

    private int statusCode=409;



    public RoutingError(String message) {
        super(message);
    }


    public RoutingError(int statusCode,String message) {
        super(message);
        this.statusCode = statusCode;
    }
    public RoutingError(String message,int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

}

package libs.customErrors;

public class MessageException extends RuntimeException {

    public MessageException(String message) {
        super(message);
    }

}

package libs;

import io.vertx.core.Vertx;
import libs.configFactory.ConfigHelper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public enum TemplateLoader {

    INSTANCE;

    private Map<String, String> template;

    public void init(Vertx vertx){

        try {
            template = this.getFiles(ConfigHelper.INSTANCE.getMainConfig().getString("template_path"), null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(template.isEmpty()) {
            System.out.println("Alert :: there is no template to load ...");
        }

    }

    private Map<String, String> getFiles(String directory, Map<String, String> files) throws IOException {
        if(files == null ){
            files = new HashMap<>();
        }

        File dir = new File(directory);

        File[] fList = dir.listFiles();

        if(fList != null)
            for (File file : fList) {
                if (file.isFile()) {
                    files.put(file.getCanonicalPath().substring(file.getCanonicalPath().lastIndexOf("template")), new String(Files.readAllBytes(Paths.get(file.getCanonicalPath()))));
                } else if (file.isDirectory()) {
                    getFiles(file.getAbsolutePath(), files);
                }
            }

        return files;
    }

    public Map<String, String> getTemplate() {
        return template;
    }
}

package libs;


import io.vertx.rxjava.ext.web.RoutingContext;
import libs.customErrors.RoutingError;

@FunctionalInterface
public interface CommonController {

    void handle(RoutingContext routingContext);

    default void fail(String msg) throws RuntimeException{
        throw new RoutingError(msg);
    }
}

package libs.utils;

import lombok.Builder;
import lombok.Data;

import java.util.function.Predicate;

@Data
@Builder
public class RequestItem {

    private String key;
    @Builder.Default
    private RequestItemType itemType=RequestItemType.STRING;
    @Builder.Default
    private boolean required=true;
    private String error;
    private Predicate<Object> predicate;
    private Class objectClass;

    public RequestItemType getItemType() {
        if(itemType==null){
            itemType = RequestItemType.STRING;
        }
        return itemType;
    }

    public RequestItem(String key, String error) {
        this.key = key;
        this.error = error;
    }
    public RequestItem(String key, String error,RequestItemType itemType) {
        this.key = key;
        this.itemType = itemType;
        this.error = error;
    }
    public RequestItem(String key, RequestItemType itemType) {
        this.key = key;
        this.required = false;
        this.itemType = itemType;
    }

    public RequestItem(String key, RequestItemType itemType, boolean required, String error, Predicate<Object> predicate, Class objectClass) {
        this.key = key;
        this.itemType = itemType;
        this.required = required;
        this.error = error;
        this.predicate = predicate;
        this.objectClass = objectClass;
    }
}

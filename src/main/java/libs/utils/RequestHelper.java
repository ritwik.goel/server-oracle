package libs.utils;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.http.HttpServerRequest;
import io.vertx.rxjava.ext.web.RoutingContext;
import libs.customErrors.RoutingError;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public enum RequestHelper {

    INSTANCE;

    public HashMap<String,Boolean> annotations(RoutingContext routingContext, String... defaults){
        List<String> annotations = routingContext.queryParam("annotations");
        HashMap<String,Boolean> hashMap = new HashMap<>();
        if(annotations==null || annotations.size()==0){
            for (String aDefault : defaults) {
                hashMap.put(aDefault,true);
            }
        }else{
            for (String annotation : annotations) {
                hashMap.put(annotation,true);
            }
        }
        return hashMap;
    }

    public RequestZipped requestZipped(RoutingContext routingContext, List<RequestItem> items) {
        if (routingContext.request().method().equals(HttpMethod.GET) || routingContext.request().method().equals(HttpMethod.DELETE))
            return new RequestZipped(routingContext, mapGetRequest(routingContext, items));
        else
            return new RequestZipped(routingContext, mapJsonRequest(routingContext.getBodyAsJson(), items));
    }

    public Request mapGetRequest(RoutingContext context, List<RequestItem> requestItems) {
        Request jsonRequest = new Request();
        HttpServerRequest request = context.request();
        for (RequestItem requestItem : requestItems) {
            Object item = null;
            switch (requestItem.getItemType()) {
                case DATE:
                    try {
                        item = Long.valueOf(request.getParam(requestItem.getKey()));
                    } catch (NumberFormatException | ClassCastException e) {
                        throw new RoutingError(String.format("Provided value for key %s is not a valid timestamp", requestItem.getKey()),  409);
                    }
                    break;
                case EMAIL:
                    String mEmail = request.getParam(requestItem.getKey());
                    if(mEmail==null && requestItem.isRequired()){
                        throw new RoutingError(String.format("No value provided for key %s.", requestItem.getKey()),  409);
                    }
                    if(mEmail!=null && !validateEmailAddress(mEmail)){
                        throw new RoutingError(String.format("Provided value for key %s is not a valid email address", requestItem.getKey()),  409);
                    }
                    item = mEmail;
                    break;
                case INTEGER:
                    try {
                        item = Long.valueOf(request.getParam(requestItem.getKey()));
                    } catch (NumberFormatException | ClassCastException e) {
                        throw new RoutingError(String.format("Provided value for key %s is not a valid number", requestItem.getKey()),  409);
                    }
                    break;
                case DOUBLE:
                    try {
                        item = Long.valueOf(request.getParam(requestItem.getKey()));
                    } catch (NumberFormatException | ClassCastException e) {
                        throw new RoutingError(String.format("Provided value for key %s is not a valid double", requestItem.getKey()),  409);
                    }
                    break;
                case STRING:
                default:
                    item = request.getParam(requestItem.getKey());
                    break;
            }
            if (item != null) {
                jsonRequest.put(requestItem.getKey(), item);
            } else {
                if (requestItem.isRequired()) {
                    throw new RoutingError(String.format("Null value provided for key %s.", requestItem.getKey()),  409);
                }
            }
        }
        return jsonRequest;
    }

    private Pattern emailPattern;

    public Pattern getEmailPattern() {
        if(emailPattern==null){
            emailPattern = Pattern.compile("^[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-z)]+\\.[(a-zA-z)]{2,3}$");
        }
        return emailPattern;
    }


    private boolean validateEmailAddress(String emailAddress) {
        return getEmailPattern().matcher(emailAddress).matches();
    }


    public Request mapJsonRequest(JsonObject mRequest, List<RequestItem> requestItems) {
        Request jsonRequest = new Request();
        if (mRequest == null) {
            throw new RoutingError("Invalid json body", 409);
        }
        for (RequestItem mRequestItem : requestItems) {
            Object item = null;
            String key = mRequestItem.getKey();
            JsonObject request = mRequest;
            String[] split = key.split("\\.");
            for(int i=0;i<split.length;i++){
                key = split[i];
                if(i==split.length-1){
                    break;
                }else{
                    if(mRequest.getValue(split[i])!=null){
                        request = mRequest.getJsonObject(split[i]);
                    }else{
                        if(mRequestItem.isRequired()){
                            throw new RoutingError(String.format("Invalid value provided for %s", mRequestItem.getKey().toLowerCase()),  409);
                        }
                    }
                }
            }
            switch (mRequestItem.getItemType()) {
                case ARRAY:
                    item  = request.getJsonArray(key);
                    break;
                case DATE:
                    try {
                        item = request.getLong(key);
                    } catch (NumberFormatException | ClassCastException e) {
                        throw new RoutingError(String.format("Provided value for key %s is not a valid timestamp", key),  409);
                    }
                    break;
                case EMAIL:
                    String mEmail = request.getString(key);
                    if(mEmail==null && mRequestItem.isRequired()){
                        throw new RoutingError(String.format("No value provided for key %s.",key),  409);
                    }
                    if(mEmail!=null && !validateEmailAddress(mEmail)){
                        throw new RoutingError(String.format("Provided value for key %s is not a valid email address", key),  409);
                    }
                    item = mEmail;
                    break;
                case BOOLEAN:
                    item = request.getBoolean(key);
                    break;
                case PASSWORD:
                    String mPassword = request.getString(key);
                    if((mPassword==null || mPassword.length()<6) && mRequestItem.isRequired()){
                        throw new RoutingError(String.format("No value provided for key %s.",key),  409);
                    }
                    item = mPassword;
                    break;
                case INTEGER:
                    try {
                        item = request.getLong(key);
                    } catch (NumberFormatException | ClassCastException e) {
                        throw new RoutingError(String.format("Provided value for key %s is not a valid number", key),  409);
                    }
                    break;
                case DOUBLE:
                    try {
                        item = request.getDouble(key);
                    } catch (NumberFormatException | ClassCastException e) {
                        throw new RoutingError(String.format("Provided value for key %s is not a valid double", key),  409);
                    }
                    break;
                case OBJECT:
                    JsonObject object  = request.getJsonObject(key);
                    item = Mapper.INSTANCE.getGson().fromJson(object.toString(),mRequestItem.getObjectClass());
                    break;
                case JSONOBJECT:
                    item = request.getJsonObject(key);
                    break;
                case STRING:
                default:
                    item = request.getString(key);
                    break;
            }
            if (item != null) {
                if(mRequestItem.getPredicate()!=null){
                    if(!mRequestItem.getPredicate().test(item)){
                        throw new RoutingError("Precondition failed",409);
                    }
                }
                jsonRequest.put(mRequestItem.getKey(), item);
            } else {
                if (mRequestItem.isRequired()) {
                    throw new RoutingError(String.format("Null value provided for key %s.", mRequestItem.getKey()),  409);
                }
            }
        }
        return jsonRequest;
    }

}

package libs.utils;

public enum RequestItemType {

    INTEGER,
    STRING,
    DOUBLE,
    EMAIL,
    OBJECT,
    JSONOBJECT,
    BOOLEAN,
    PASSWORD,
    ARRAY,
    DATE;

}

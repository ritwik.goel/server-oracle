package libs.utils;

import java.security.SecureRandom;
import java.util.Random;

public class RandomString {

    private static RandomString i;

    public static RandomString i() {
        if (i == null) {
            i = new RandomString();
        }
        return i;
    }

    private String nextString() {
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }

    private Random random;

    private char[] symbols;

    private char[] buf;

    private RandomString() {
        String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String digits = "0123456789";
        String alphanum = upper + digits;
        this.random = new SecureRandom();
        this.symbols = alphanum.toCharArray();
        this.buf = new char[9];
    }

    public String random() {
        return nextString();
    }

}
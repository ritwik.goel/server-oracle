package libs.utils;

import io.vertx.core.json.JsonObject;

public class Lodash {

    public static <T>T get(JsonObject object,String... args){
        JsonObject jsonObject = object;
        String lastKey = "";
        for(int i=0;i<args.length;i++){
            lastKey = args[i];
            if(args.length-1 == i){
                break;
            }
            if(object.containsKey(lastKey)){
                jsonObject = object.getJsonObject(lastKey) ;
            }else{
                return null;
            }
        }
        if(jsonObject.containsKey(lastKey)){
            return (T)jsonObject.getValue(lastKey);
        }else{
            return null;
        }
    }

    public static <T>T getOrDefault(JsonObject object,T t,String... args){
        JsonObject jsonObject = object;
        String lastKey = "";
        for(int i=0;i<args.length;i++){
            lastKey = args[i];
            if(args.length-1 == i){
                break;
            }
            if(object.containsKey(lastKey)){
                jsonObject = object.getJsonObject(lastKey) ;
            }else{
                return t;
            }
        }
        if(jsonObject.containsKey(lastKey)){
            return (T)jsonObject.getValue(lastKey);
        }else{
            return t;
        }
    }

}

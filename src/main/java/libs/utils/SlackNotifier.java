package libs.utils;

import com.mashape.unirest.http.Unirest;
import io.vertx.core.json.JsonObject;
import libs.configFactory.ConfigHelper;
import libs.configFactory.ConfigManager;

public enum SlackNotifier {

    INSTANCE;

    public void hitSlack(String text, String url){

        JsonObject request = new JsonObject()
                .put("text", text);

        if(!ConfigManager.INSTANCE.getMainConfig().getString("environment").equals("staging")){
            try{
                Unirest.post(url)
                        .header("Content-Type", "application/json")
                        .body(request.toString())
                        .asString();
            }catch(Exception e){
                System.out.println("ERROR : [Slack][EkaSyncUpdate] Exception Occurred");
                e.printStackTrace();
            }
        }
    }

    public void hitSlackCommon(String text){

        JsonObject request = new JsonObject()
                .put("text", text);

        if(!ConfigManager.INSTANCE.getMainConfig().getString("environment").equals("staging")){
            try{
                Unirest.post(ConfigHelper.INSTANCE.getSlackConfig().getString("ekaSyncUpdate"))
                        .header("Content-Type", "application/json")
                        .body(request.toString())
                        .asString();
            }catch(Exception e){
                System.out.println("ERROR : [Slack][EkaSyncUpdate] Exception Occurred");
                e.printStackTrace();
            }
        }
    }
}

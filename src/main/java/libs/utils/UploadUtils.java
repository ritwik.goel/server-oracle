package libs.utils;

import apis.ApisManager;
import apis.blackhole.response.FileUploadResponse;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

import java.io.File;

public enum UploadUtils {

    INSTANCE;

    public String upload(String filePath, String filename, File file) {
        try {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part mBody = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
            Call<FileUploadResponse> fileUploadResponseCall = ApisManager.INSTANCE.getFileUploadApis().upload(mBody, filePath, filename);
            Response<FileUploadResponse> fileUploadResponse = fileUploadResponseCall.execute();
            if (!fileUploadResponse.isSuccessful())
                return null;
            FileUploadResponse fileUpload = fileUploadResponse.body();
            if (!fileUpload.getStatus().equalsIgnoreCase("success"))
                return null;
            file.delete();
            return fileUpload.getUrl();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error !!! Upload Failed");
            return null;
        }
    }
}
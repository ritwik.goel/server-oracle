package libs.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public enum DateService {

    INSTANCE;

    private SimpleDateFormat dateHHFormatForFileUpload;
    public SimpleDateFormat getDateHHFormatForFileUpload(){
        if (dateHHFormatForFileUpload == null){
            dateHHFormatForFileUpload = new SimpleDateFormat("dd_MM_yyyy_HH");
            dateHHFormatForFileUpload.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return dateHHFormatForFileUpload;
    }

    private SimpleDateFormat dateFormatForFileUpload;
    public SimpleDateFormat getDateFormatForFileUpload(){
        if (dateFormatForFileUpload == null){
            dateFormatForFileUpload = new SimpleDateFormat("dd_MM_yyyy");
            dateFormatForFileUpload.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return dateFormatForFileUpload;
    }

    private SimpleDateFormat dateFormat;
    public SimpleDateFormat getDateTimeFormat(){
        if (dateFormat == null){
            dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return dateFormat;
    }

    private SimpleDateFormat reverseDateFormat;
    public SimpleDateFormat getReverseDateTimeFormat(){
        if (reverseDateFormat == null){
            reverseDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            reverseDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return reverseDateFormat;
    }

    private SimpleDateFormat timeFormat;
    public SimpleDateFormat getTimeFormat(){
        if (timeFormat == null){
            timeFormat = new SimpleDateFormat("HH:mm");
            timeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return timeFormat;
    }

    private SimpleDateFormat dateFormatWithTime;
    public SimpleDateFormat getDateFormatWithTime(){
        if (dateFormatWithTime == null){
            dateFormatWithTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            dateFormatWithTime.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return dateFormatWithTime;
    }

    private SimpleDateFormat ledgerDateFormat;
    public SimpleDateFormat getLedgerDateFormat(){
        if (ledgerDateFormat == null){
            //2020-11-23 11:32:19.219000
            ledgerDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss'.000000'");
            ledgerDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return ledgerDateFormat;
    }

    private SimpleDateFormat dateFormatThreeLetterMonth;
    public SimpleDateFormat getThreeLetterMonthDateFormat(){
        if (dateFormatThreeLetterMonth == null){
            dateFormatThreeLetterMonth = new SimpleDateFormat("dd-MMM-yyyy");
            dateFormatThreeLetterMonth.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return dateFormatThreeLetterMonth;
    }

    private SimpleDateFormat dateFormatSmallThreeLetterMonth;
    public SimpleDateFormat getDateFormatSmallThreeLetterMonth(){
        if (dateFormatSmallThreeLetterMonth == null){
            dateFormatSmallThreeLetterMonth = new SimpleDateFormat("dd-MMM-yy");
            dateFormatSmallThreeLetterMonth.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return dateFormatSmallThreeLetterMonth;
    }

    private SimpleDateFormat workLineDateFormat;
    public SimpleDateFormat getWorkLineDateTimeFormat(){
        if (workLineDateFormat == null){
            workLineDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            workLineDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return workLineDateFormat;
    }

    private SimpleDateFormat quickbooksDateFormat;
    public SimpleDateFormat getQuickbooksDateTimeFormat(){
        if (quickbooksDateFormat == null){
            quickbooksDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            quickbooksDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        return quickbooksDateFormat;
    }

    private SimpleDateFormat olaDateFormat;
    public SimpleDateFormat getOlaDateTimeFormat(){
        if (olaDateFormat == null){
            olaDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            olaDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return olaDateFormat;
    }

    private SimpleDateFormat dateFormatDtdcRamco;
    public SimpleDateFormat getDateFormatDtdcRamco(){
        if (dateFormatDtdcRamco == null){
            // Dec  1 2015 12:00AM
            dateFormatDtdcRamco = new SimpleDateFormat("MMM d yyyy HH:mmaa");
            dateFormatDtdcRamco.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return dateFormatDtdcRamco;
    }

    private SimpleDateFormat dateFormatCjDarclSap;
    public SimpleDateFormat getDateFormatCjDarclSap(){
        if (dateFormatCjDarclSap == null){
            dateFormatCjDarclSap = new SimpleDateFormat("dd.MM.yyyy");
            dateFormatCjDarclSap.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return dateFormatCjDarclSap;
    }

    private SimpleDateFormat dateFormatCjDarclSapYYYYMMDD;
    public SimpleDateFormat getDateFormatCjDarclSapYYYYMMDD(){
        if (dateFormatCjDarclSapYYYYMMDD == null){
            dateFormatCjDarclSapYYYYMMDD = new SimpleDateFormat("yyyyMMdd");
            dateFormatCjDarclSapYYYYMMDD.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return dateFormatCjDarclSapYYYYMMDD;
    }

    private SimpleDateFormat dateFormatDtdcRamcoFile;
    public SimpleDateFormat getDateFormatDtdcRamcoFile(){
        if (dateFormatDtdcRamcoFile == null){
            dateFormatDtdcRamcoFile = new SimpleDateFormat("yyyymmmd");
            dateFormatDtdcRamcoFile.setTimeZone(TimeZone.getTimeZone("IST"));
        }
        return dateFormatDtdcRamcoFile;
    }

    public String timestampToDateString(Long timestamp){
        try {
            Date date = new Date(timestamp);
            return getDateTimeFormat().format(date);
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    public String timestampToTimeString(Long timestamp){
        try {
            Date date = new Date(timestamp);
            return getTimeFormat().format(date);
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    public String timestampToDateTimeString(Long timestamp){
        try {
            Date date = new Date(timestamp);
            return getDateFormatWithTime().format(date);
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    public Date getMidnightDate(Date date){
        Long time = date.getTime();
        return new Date(time - time % (24 * 60 * 60 * 1000));
    }
}

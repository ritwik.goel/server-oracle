package libs.utils;

import io.vertx.rxjava.ext.web.RoutingContext;
import libs.customErrors.RoutingError;
import lombok.AllArgsConstructor;
import lombok.Data;

public enum ResponseHelper {

    INSTANCE;

    public void writeJsonResponse(RoutingContext routingContext, Object o){
       try {
           routingContext.response().putHeader("content-type","application/json")
                   .end(Mapper.INSTANCE.getGson().toJson(o));
       }catch (IllegalStateException e){
            e.printStackTrace();
            this.handleError(routingContext, e);
       }
    }

    public void writeError(RoutingContext routingContext, int statusCode, String message){
        if(!routingContext.response().closed()){
            routingContext.put("error",message);
            routingContext.response().putHeader("content-type","application/json")
                    .setStatusCode(statusCode).end(new Error(message).toString());
        }
    }

    public void handleError(RoutingContext rc, Throwable error){
        try {
            if(error.getClass().isAssignableFrom(RoutingError.class) && (error instanceof RoutingError)){
                writeError(rc,((RoutingError) error).getStatusCode(),error.getMessage());
            }else{
                error.printStackTrace();
                writeError(rc,413,"Internal server error.Please try again later");
            }
        }catch (Exception e){
            e.printStackTrace();
            writeError(rc,409,e.getMessage());
        }
    }



    @Data
    @AllArgsConstructor
    class Error{

        private String message;

        public String toString(){
            return String.format("{\"message\":\"%s\"}",message);
        }

    }

}

package libs.utils;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import io.vertx.core.buffer.Buffer;
import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.UUID;

public enum FilesUtils {

    INSTANCE;

    public Buffer getNetworkFile(String fileUrl){
        Buffer buffer = Buffer.buffer();
        try (BufferedInputStream in = new BufferedInputStream(new URL(fileUrl.replaceAll(" ","%20")).openStream())){
            byte dataBuffer[] = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                buffer.appendBytes(dataBuffer, 0, bytesRead);
            }
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
        return buffer;
    }

    public File getNetworkFileOrg(String fileUrl) {
        try {
            System.out.println("fileUrl :: "+fileUrl);
            HttpResponse<String> response = Unirest.get(fileUrl).asString();
            if (response.getStatus() != 200){
                return null;
            }
            File tempFile = File.createTempFile(UUID.randomUUID().toString(), fileUrl.substring(fileUrl.lastIndexOf(".")));
            try (FileOutputStream fileOutputStream = new FileOutputStream(tempFile)) {
                IOUtils.copy(response.getRawBody(), fileOutputStream);
            }
            return tempFile;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
package libs.utils;

public interface RegexFormulas {

    public final String CSVDELIMETERWITHCOMMA = ",(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)";

    public final String PSVDELIMETERWITHCOMMA = "|(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)";
}

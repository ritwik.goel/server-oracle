package libs.utils;

import apis.ApisManager;
import apis.blackhole.response.FileUploadResponse;
import com.opencsv.CSVWriter;
import database.models.Plugins;
import io.ebean.plugin.Plugin;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public enum CsvUploadUtils {

    INSTANCE;

    public String writeCsvFile(String filePath, List<String[]> data, String filename){

        File file = new File(filePath);
        try {
            File directory = new File(file.getPath().substring(0,file.getPath().lastIndexOf('/')+1));
            if (!directory.exists()){
                directory.mkdirs();
            }
            FileWriter outputfile = new FileWriter(file);

            CSVWriter writer = new CSVWriter(outputfile,',',
                    CSVWriter.DEFAULT_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);

            writer.writeAll(data);

            writer.close();

            return uploadCsv("assets/temp/slug/csv/", filename,file);
        }catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public File writeCsvFileOnly(String filePath, List<String[]> data){

        File file = new File(filePath);
        try {
            File directory = new File(file.getPath().substring(0,file.getPath().lastIndexOf('/')+1));
            if (!directory.exists()){
                directory.mkdirs();
            }
            FileWriter outputfile = new FileWriter(file);

            CSVWriter writer = new CSVWriter(outputfile,',',
                    CSVWriter.DEFAULT_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);

            writer.writeAll(data);

            writer.close();

            return file;
        }catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String uploadCsv(String filePath, String filename, File pdfFile) {
        try {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), pdfFile);
            MultipartBody.Part mBody = MultipartBody.Part.createFormData("file", pdfFile.getName(), requestFile);
            Call<FileUploadResponse> fileUploadResponseCall = ApisManager.INSTANCE.getFileUploadApis().upload(mBody, filePath, filename);
            Response<FileUploadResponse> fileUploadResponse = fileUploadResponseCall.execute();
            if (!fileUploadResponse.isSuccessful())
                return null;
            FileUploadResponse fileUpload = fileUploadResponse.body();
            if (!fileUpload.getStatus().equalsIgnoreCase("success"))
                return null;
            pdfFile.delete();
            return fileUpload.getUrl();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error !!! Upload Failed");
            return null;
        }
    }

    public String getPluginBucketFilePath(Plugins plugin){
        return String.format("plugin/integration/%s/hrms/%s/", plugin.getCode(), DateService.INSTANCE.getDateFormatForFileUpload().format(new Date()));
    }
}
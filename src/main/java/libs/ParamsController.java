package libs;

import apis.ApisManager;
import apis.eka.Request.AuthenticateRequest.AuthenticationRequest;
import apis.eka.Request.InstallationRequest.InstallationRequest;
import apis.eka.Response.AuthResponse.AuthenticationResponse;
import apis.eka.Response.AuthResponse.InstallationResponse;
import io.vertx.rxjava.core.RxHelper;
import io.vertx.rxjava.ext.web.RoutingContext;
import libs.configFactory.ConfigHelper;
import libs.customErrors.RoutingError;
import libs.utils.RequestHelper;
import libs.utils.RequestItem;
import libs.utils.RequestZipped;
import retrofit2.Call;
import rx.Single;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;

public interface ParamsController extends CommonController{

   List<RequestItem> items();

   default RequestZipped map(RoutingContext event){
       return RequestHelper.INSTANCE.requestZipped(event,items());
   }

   default RequestZipped developerAuth(RoutingContext routingContext){
       String clientId = routingContext.request().getHeader("client-id");
       String clientSecret = routingContext.request().getHeader("client-secret");
       if (clientId == null || clientSecret == null){
           fail("Client Id and Client Secret is required.");
       }
       if (clientId.trim().equals("") || !clientId.equals(ConfigHelper.INSTANCE.getApplicationConfig().getString("id"))){
           fail("Invalid Client Id");
       }
       if (clientSecret.trim().equals("") || !clientSecret.equals(ConfigHelper.INSTANCE.getApplicationConfig().getString("secret"))){
           fail("Invalid Client Secret");
       }
       return RequestHelper.INSTANCE.requestZipped(routingContext,items());
   }

    default RequestZipped withAuthentication(RoutingContext routingContext){
       String header = routingContext.request().getHeader("Authorization");
       if (header == null)
           throw new RoutingError("Invalid header");
       AuthenticationRequest request = new AuthenticationRequest();
       request.setToken(header);
       Call<AuthenticationResponse> responseCall = ApisManager.INSTANCE.getEkaAPis().authenticateUser(request);
       retrofit2.Response<AuthenticationResponse> mR = null;
       try {
           mR = responseCall.execute();
           if (mR.isSuccessful()) {
               AuthenticationResponse authenticationResponse = mR.body();
               String companyCode = authenticationResponse.getCode();
               if (companyCode == null || companyCode.equalsIgnoreCase(""))
                   throw new RoutingError("No Company Details found");
               routingContext.put("code",companyCode);
               return RequestHelper.INSTANCE.requestZipped(routingContext,items());
           }else {
               throw new RoutingError(mR.code(),mR.errorBody().string());
           }
       } catch (IOException e) {
           throw new RoutingError(e.getMessage());
       }
    }

    default InstallationResponse install(RequestZipped zipped, String type, HashSet<String> roles){
        String header = zipped.getRoutingContext().request().getHeader("Authorization");
        InstallationRequest request =new InstallationRequest();
        request.setToken(header);
        request.setRoles(roles);
        request.setPlugin(type);
        Call<InstallationResponse> responseCall = ApisManager.INSTANCE.getEkaAPis().installPlugin(request);
        retrofit2.Response<InstallationResponse> mR = null;
        try {
            mR = responseCall.execute();
            if (mR.isSuccessful()) {
                return mR.body();
            }else {
                throw new RoutingError(mR.code(),mR.errorBody().string());
            }
        } catch (IOException e) {
            throw new RoutingError(e.getMessage());
        }
    }

    default Single<RequestZipped> authenticated(RoutingContext routingContext){
       return Single.just(routingContext)
               .subscribeOn(RxHelper.blockingScheduler(routingContext.vertx()))
               .map(this::withAuthentication);
   }

   default RequestZipped serverAuthentication(RoutingContext routingContext){
       return RequestHelper.INSTANCE.requestZipped(routingContext,items());
   }

   default String getCode(RequestZipped zipped){
       return zipped.getRoutingContext().get("code");
   }

}
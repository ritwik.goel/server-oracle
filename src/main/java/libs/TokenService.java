package libs;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.sentry.Sentry;
import libs.configFactory.ConfigHelper;
import libs.customErrors.RoutingError;
import oauth.design.Oauth;
import org.apache.commons.lang3.time.DateUtils;
import plugins.mailApprovals.repos.MailsRepository;

import java.io.UnsupportedEncodingException;
import java.util.Date;

public enum TokenService {

    INSTANCE;

    public DecodedJWT decodedJWT(String token){
        JWTVerifier verifier;
        try {
            verifier = JWT.require(Oauth.Algo()).withIssuer(ConfigHelper.INSTANCE.getApplicationConfig().getString("issuer")).build();
        }catch (UnsupportedEncodingException e){
            throw new RoutingError(401,"Unsupported Encoding");
        }
        DecodedJWT jwt;
        try {
            jwt = verifier.verify(token);
            if (MailsRepository.INSTANCE.checkToken(token) != null){
                throw new RoutingError(409,"Invalid Request. Already submitted or blacklisted token.");
            }
        } catch (TokenExpiredException e) {
            throw new RoutingError(401,"Token is expired");
        } catch (JWTVerificationException e) {
            throw new RoutingError(401,"Token is malformed");
        }
        return jwt;
    }

    public DecodedJWT decodedJWTNIC(String token){
        DecodedJWT jwt;
        try {
            jwt = JWT.decode(token);
        } catch (TokenExpiredException e) {
            throw new RoutingError(401,"Token is expired");
        } catch (JWTVerificationException e) {
            throw new RoutingError(401,"Token is malformed");
        }
        return jwt;
    }

    public String confirmationToken(Long id, String approveEmail, String approveKey){
        Date date = new Date();
        Date newDate = DateUtils.addMonths(date, 30);
        Algorithm algorithm ;
        try {
            algorithm = Oauth.Algo();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Sentry.captureException(e);
            throw new RoutingError(500,"Internal server error.Please try again later");
        }
        String token = JWT.create()
                .withIssuer(ConfigHelper.INSTANCE.getApplicationConfig().getString("issuer"))
                .withExpiresAt(newDate)
                .withClaim("type", "approval_token")
                .withClaim("email_id", id) // 1, 2, 3
                .withClaim("approve_key", approveKey) // approver 1 , approver 2
                .withClaim("approveEmail", approveEmail)
                .sign(algorithm);
        return token;
    }
}
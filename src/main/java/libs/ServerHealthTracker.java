package libs;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.net.InetAddress;
import java.util.concurrent.CompletableFuture;

public enum ServerHealthTracker {

    INSTANCE;

    private String StatusId = null;
    private String ServerType = "plugin";

    public void init(Vertx vertx){
        vertx.setPeriodic(60 * 1000, this::run);
    }

    private void run(long event){
        try {
            if (StatusId != null || (StatusId = find()) != null){
                update(StatusId);
            }else {
                create();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void create(){
        try {
            Runtime runtime = Runtime.getRuntime();
            InetAddress localHost = InetAddress.getLocalHost();
            JsonObject request = new JsonObject();
            JsonObject data = new JsonObject();
            data.put("server_type",ServerType);
            data.put("host_name",localHost.getHostName());
            data.put("host_address",localHost.getHostAddress());
            data.put("proccessor",runtime.availableProcessors());
            data.put("totalMemory",runtime.totalMemory());
            data.put("freeMemory",runtime.freeMemory());
            data.put("activeThread",Thread.activeCount());
            data.put("usedMemory",runtime.totalMemory()-runtime.freeMemory());
            data.put("created_at",System.currentTimeMillis());
            data.put("updated_at",System.currentTimeMillis());
            request.put("data", data);
            HttpResponse<String> rawResponse = Unirest.post("https://5f96e591c9e77c000655fde9.tables.business")
                    .header("X-CLIENT-ID", "5f96e62ec9e77c0006bebd48")
                    .header("X-CLIENT-SECRET", "783bb924539549e5befb10a7f47b5ce0")
                    .header("Content-Type", "application/json")
                    .body(request.toString())
                    .asString();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void update(String id){
        try {
            Runtime runtime = Runtime.getRuntime();
            InetAddress localHost = InetAddress.getLocalHost();
            JsonObject request = new JsonObject();
            JsonObject data = new JsonObject();
            data.put("server_type",ServerType);
            data.put("host_name",localHost.getHostName());
            data.put("host_address",localHost.getHostAddress());
            data.put("proccessor",runtime.availableProcessors());
            data.put("totalMemory",runtime.totalMemory());
            data.put("freeMemory",runtime.freeMemory());
            data.put("activeThread",Thread.activeCount());
            data.put("usedMemory",runtime.totalMemory()-runtime.freeMemory());
            data.put("updated_at",System.currentTimeMillis());
            request.put("data", data);
            HttpResponse<String> rawResponse = Unirest.patch("https://5f96e591c9e77c000655fde9.tables.business/"+id)
                    .header("X-CLIENT-ID", "5f96e62ec9e77c0006bebd48")
                    .header("X-CLIENT-SECRET", "783bb924539549e5befb10a7f47b5ce0")
                    .header("Content-Type", "application/json")
                    .body(request.toString())
                    .asString();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String find(){
        try {
            InetAddress localHost = InetAddress.getLocalHost();
            JsonObject request = new JsonObject();
            JsonObject query = new JsonObject();
            query.put("server_type",ServerType);
            query.put("host_name",localHost.getHostName());
            request.put("query", query);
            request.put("limit", 1);
            HttpResponse<String> rawResponse = Unirest.post("https://5f96e591c9e77c000655fde9.tables.business/filter")
                    .header("X-CLIENT-ID", "5f96e62ec9e77c0006bebd48")
                    .header("X-CLIENT-SECRET", "783bb924539549e5befb10a7f47b5ce0")
                    .header("Content-Type", "application/json")
                    .body(request.toString())
                    .asString();
            JsonObject jsonObject = new JsonObject(rawResponse.getBody());
            JsonArray jsonArray = jsonObject.getJsonArray("data");
            if (jsonArray != null && jsonArray.size() > 0){
                JsonObject item = jsonArray.getJsonObject(0);
                return item.getString("id");
            }
            return null;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
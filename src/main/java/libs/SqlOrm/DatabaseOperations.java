package libs.SqlOrm;

import libs.SqlOrm.QueryExpressions.QueryBuilder;
import libs.utils.Mapper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DatabaseOperations<T extends BaseTable> {

    private String TABLE_NAME;
    private Class<T> tClass;

    public DatabaseOperations(Class<T> clz) {
        this.TABLE_NAME = OperationsHelper.INSTANCE.tableName(clz);
        tClass = clz;
    }

    public QueryBuilder<T> query(){
        return new QueryBuilder<T>(tClass);
    }


    public T byEkaId(Connection connection, Long id) throws SQLException {
        String query = "Select * from " + TABLE_NAME + " where eka_id = ? and deleted = 0";
        SqlDebugger.INSTANCE.print(query);
        return getMappedObject(connection, id, query);
    }


    public T byId(Connection connection, Long id) throws SQLException {
        String query = "Select * from " + TABLE_NAME + " where id = ? and deleted = 0";
        SqlDebugger.INSTANCE.print(query);
        return getMappedObject(connection, id, query);
    }

    private T getMappedObject(Connection connection, Long id, String query) throws SQLException {
        try(PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setLong(1, id);
            ResultSet resultSet = stmt.executeQuery();
            Collection<T> collection = OperationsHelper.INSTANCE.collection(resultSet,tClass);
            if(collection.size()==0)
                return null;
            else
                return collection.iterator().next();
        }catch (SQLException e){
            throw e;
        }
    }


    public void delete(Connection connection, T object) throws SQLException {
        String query = "Update " + TABLE_NAME + " set deleted = 1 where id = ?";
        try(PreparedStatement stmt = connection.prepareStatement(query);){
            stmt.setLong(1, object.getId());
            stmt.execute();
        }catch (SQLException e){
            throw e;
        }
    }

    public void create(Connection connection, T object) throws SQLException {
        Method[] methods = object.getClass().getMethods();
        List<String> keys = new ArrayList<>();
        List<Object> values = new ArrayList<>();
        getFields((T) object, methods, keys, values);
        StringBuilder builder = new StringBuilder();
        builder.append("Insert into ")
                .append(TABLE_NAME)
                .append("(");


        for (int i = 0; i < keys.size(); i++) {
            if (i == 0) {
                builder.append(keys.get(i));
            } else {
                builder.append(",");
                builder.append(keys.get(i));
            }
        }
        builder.append(") values(");
        for (int i = 0; i < keys.size(); i++) {
            if (i == 0) {
                builder.append("?");
            } else {
                builder.append(",");
                builder.append("?");
            }
        }
        builder.append(")");
        String query = builder.toString();

        SqlDebugger.INSTANCE.print(query);

        try( PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            addParameters(values, statement);
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            rs.next();
            Long id = (long)rs.getInt(1);
            rs.close();
            object.setId(id);
        }catch (SQLException e){
            throw e;
        }
    }

    public void update(Connection connection, T object) throws SQLException {
        Method[] methods = object.getClass().getMethods();
        List<String> keys = new ArrayList<>();
        List<Object> values = new ArrayList<>();
        getFields((T) object, methods, keys, values);
        StringBuilder builder = new StringBuilder();
        builder.append("update ")
                .append(TABLE_NAME)
                .append(" set ");

        for (int i = 0; i < keys.size(); i++) {
            if(i!=0){
                builder.append(",");
            }
            builder.append(keys.get(i)).append(" = ? ");
        }

        builder.append("where id = ?");

        String query = builder.toString();
        SqlDebugger.INSTANCE.print(query);


        try( PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(addParameters(values, statement), object.getId());
            statement.executeUpdate();
        }catch (SQLException e){
            throw e;
        }
    }

    private void getFields(T object, Method[] methods, List<String> keys, List<Object> values) throws SQLException {
        for (Method method : methods) {
            if ((method.getName().startsWith("get") || method.getName().startsWith("is")) && method.getParameterTypes().length == 0
                    && !method.isAnnotationPresent(QueryIgnore.class)) {
                String name = method.getName().startsWith("get") ? OperationsHelper.INSTANCE.getFieldName(method.getName().substring(3)) : OperationsHelper.INSTANCE.getFieldName(method.getName().substring(2));
                try {
                    if (method.getReturnType().equals(boolean.class) || method.getReturnType().equals(Boolean.class)) {
                        values.add((Boolean) method.invoke(object));
                    } else if (method.getReturnType().equals(long.class) || method.getReturnType().equals(Long.class)) {
                        values.add((Long) method.invoke(object));
                    } else if (method.getReturnType().equals(String.class)) {
                        values.add((String) method.invoke(object));
                    } else if (method.getReturnType().equals(Date.class)) {
                        values.add((Date) method.invoke(object));
                    } else if (method.getReturnType().equals(Double.class) || method.getReturnType().equals(double.class)) {
                        values.add((Double) method.invoke(object));
                    } else {
                        Object o = method.invoke(object);
                        values.add(Mapper.INSTANCE.getGson().toJson(o));
                    }
                } catch (IllegalAccessException | UnsupportedOperationException | InvocationTargetException e) {
                    continue;
                }
                keys.add(name);
            }
        }
    }

    private int addParameters(List<Object> values, PreparedStatement statement) throws SQLException {
        int current = 1;
        for (Object value : values) {
            if (value instanceof String) {
                statement.setString(current, (String) value);
            } else if (value instanceof Double) {
                statement.setDouble(current, (Double) value);
            } else if (value instanceof Date) {
                statement.setDate(current, (Date) value);
            } else if (value instanceof Long) {
                statement.setLong(current, (Long) value);
            } else {
                statement.setString(current, value.toString());
            }
            current++;
        }
        return current;
    }


}

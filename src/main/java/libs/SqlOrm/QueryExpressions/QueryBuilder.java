package libs.SqlOrm.QueryExpressions;

import libs.SqlOrm.BaseTable;
import libs.SqlOrm.OperationsHelper;
import libs.SqlOrm.SqlDebugger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class QueryBuilder<T extends BaseTable> {

    private String SELECT_KEY = "*";

    private List<Object> objects = new ArrayList<>();

    private Class<T> cls;

    public QueryBuilder(Class<T> cls) {
        this.cls = cls;
    }

    public QueryBuilder<T> eq(String key, Object o) {
        objects.add(new Expr(key,Expressions.EQ, o));
        return this;
    }

    public QueryBuilder<T> notEq(String key, Object o) {
        objects.add(new Expr(key,Expressions.NOTEQ, o));
        return this;
    }

    public QueryBuilder<T> gt(String key, Object o) {
        objects.add(new Expr(key,Expressions.GT, o));
        return this;
    }

    public QueryBuilder<T> gte(String key, Object o) {
        objects.add(new Expr(key,Expressions.GTE, o));
        return this;
    }

    public QueryBuilder<T> lt(String key, Object o) {
        objects.add(new Expr(key,Expressions.LT, o));
        return this;
    }

    public QueryBuilder<T> in(String key, Collection<Object> o) {
        if(o.size()==0)
            return this;
        objects.add(new Expr(key,Expressions.IN, o.toArray()));
        return this;
    }

    public QueryBuilder<T> in(String key, Object... o) {
        if(o.length==0)
            return this;
        objects.add(new Expr(key,Expressions.IN, o));
        return this;
    }

    public QueryBuilder<T> lte(String key, Object o) {
        objects.add(new Expr(key,Expressions.LTE, o));
        return this;
    }


    public QueryBuilder<T> notIn(String key, Object... o) {
        if(o.length==0)
            return this;
        objects.add(new Expr(key,Expressions.NOTIN, o));
        return this;
    }

    public QueryBuilder<T> notIn(String key, Collection<Object> o) {
        if(o.size()==0)
            return this;
        objects.add(new Expr(key,Expressions.NOTIN, o.toArray()));
        return this;
    }

    public QueryBuilder<T> select(String... strings) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < strings.length; i++) {
            if (i != 0) {
                builder.append(",");
            }
            builder.append(strings[i]);
        }
        SELECT_KEY = builder.toString();
        return this;
    }

    private String toSql() {
        StringBuilder builder = new StringBuilder();
        builder.append("Select ")
                .append(SELECT_KEY)
                .append(" from ")
                .append(OperationsHelper.INSTANCE.tableName(cls))
                .append(" t0");
        if (objects.size() == 0)
            return builder.toString();
        for (int i = 0; i < objects.size(); i++) {
            if (i == 0) {
                builder.append(" where ");
            } else {
                builder.append(" and ");
            }
            Object o = objects.get(i);
            if(o instanceof Expression){
                builder.append(((Expression) o).getKey());
                switch (((Expression) o).getExpression()){
                    case EQ:
                        builder.append(" = ?");
                        break;
                    case GT:
                        builder.append(" > ?");
                        break;
                    case IN:
                        builder.append(" in ?");
                        break;
                    case LT:
                        builder.append(" < ?");
                        break;
                    case GTE:
                        builder.append(" >= ?");
                        break;
                    case LTE:
                        builder.append(" =< ?");
                        break;
                    case NOTEQ:
                        builder.append(" <> ?");
                        break;
                    case NOTIN:
                        builder.append(" not in ?");
                        break;
                }
            }
        }
        return builder.toString();
    }

    public T findOne(Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(toSql())) {
            putObjects(statement, objects);
            SqlDebugger.INSTANCE.print(statement.toString());
            ResultSet rs = statement.executeQuery();
            Collection<T> collection = OperationsHelper.INSTANCE.collection(rs,cls);
            if (collection.size() == 0)
                return null;
            else
                return collection.iterator().next();
        } catch (SQLException e) {
            throw e;
        }
    }

    public Collection<T> findList(Connection connection) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(toSql())) {
            putObjects(statement, objects);
            SqlDebugger.INSTANCE.print(statement.toString());
            ResultSet rs = statement.executeQuery();
            Collection<T> collection = OperationsHelper.INSTANCE.collection(rs,cls);
            if (collection.size() == 0)
                return new ArrayList<>();
            else
                return collection;
        } catch (SQLException e) {
            throw e;
        }
    }

    private void putObjects(PreparedStatement statement, List<Object> objects) throws SQLException {
        int current = 1;
        for (Object o : objects) {
            if (o instanceof Expression) {
                putObject(statement, current, ((Expression) o).getValue());
            }
            current++;
        }
    }

    private void putObject(PreparedStatement statement, int index, Object o) throws SQLException {
        if (o instanceof String) {
            statement.setString(index, (String) o);
        } else if (o instanceof Long) {
            statement.setLong(index, (Long) o);
        } else if (o instanceof Date) {
            statement.setDate(index, (Date) o);
        } else if (o instanceof Double) {
            statement.setDouble(index, (Double) o);
        } else if (o instanceof Object[]) {
            Object[] array = (Object[])o;
            if (array.length > 0) {
                if (array[0] instanceof String) {
                    statement.setArray(index, statement.getConnection().createArrayOf("text", array));
                } else if (array[0] instanceof Long) {
                    statement.setArray(index, statement.getConnection().createArrayOf("number", array));
                }
            }
        }
    }

}

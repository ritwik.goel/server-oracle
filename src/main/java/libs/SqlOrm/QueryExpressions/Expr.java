package libs.SqlOrm.QueryExpressions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
class Expr implements Expression{

    private String key;
    private Expressions expr;
    private Object object;

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Expressions getExpression() {
        return expr;
    }

    @Override
    public Object getValue() {
        return object;
    }
}

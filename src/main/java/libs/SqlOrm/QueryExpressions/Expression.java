package libs.SqlOrm.QueryExpressions;

public interface Expression {

    String getKey();

    Expressions getExpression();

    Object getValue();

}

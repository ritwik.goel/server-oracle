package libs.SqlOrm.QueryExpressions;

public enum Expressions {

    EQ,
    GT,
    GTE,
    NOTEQ,
    LT,
    LTE,
    IN,
    NOTIN
    ;

}

package libs.SqlOrm;

public enum  SqlDebugger {

    INSTANCE;

    private boolean debug = true;

    public void print(String s){
        if(debug){
            System.out.println("---------Executing Query---------");
            System.out.println(s);
            System.out.println("-----------------------");
        }
    }


}

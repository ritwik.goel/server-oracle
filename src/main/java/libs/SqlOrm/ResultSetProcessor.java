package libs.SqlOrm;

import java.sql.ResultSet;
import java.sql.SQLException;

interface ResultSetProcessor<T> {
    void process(T object, ResultSet resultSet) throws SQLException;
}

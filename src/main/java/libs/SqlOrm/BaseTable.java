package libs.SqlOrm;

import lombok.Getter;
import lombok.Setter;

public class BaseTable {

    private Long id;

    @Getter
    @Setter
    private Long ekaId;

    @QueryIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

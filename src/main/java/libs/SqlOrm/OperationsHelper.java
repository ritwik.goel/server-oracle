package libs.SqlOrm;

import com.google.common.base.CaseFormat;
import libs.LOGGER;
import libs.utils.Mapper;

import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public enum OperationsHelper {

    INSTANCE;


    public String tableName(Class obj) {
        String TABLE_NAME = null;
        for (Annotation annotation : obj.getAnnotations()) {
            if (annotation instanceof Table) {
                TABLE_NAME = ((Table) annotation).name();
            }
        }
        if (TABLE_NAME == null) {
            TABLE_NAME = obj.getSimpleName() + "s";
            TABLE_NAME = OperationsHelper.INSTANCE.getFieldName(TABLE_NAME);
        }
        return TABLE_NAME;
    }

    public String getFieldName(String method) {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, method);
    }


    public  <T> void addProcessors(List<ResultSetProcessor<T>> processors,final Class<?> parameterType, final Method method, final String name) {
        if (parameterType.equals(boolean.class) || parameterType.equals(Boolean.class)) {
            processors.add((object, resultSet) -> {
                try {
                    method.invoke(object, resultSet.getBoolean(name));
                } catch (IllegalAccessException | InvocationTargetException error) {
                    LOGGER.warn("Set property error", error);
                }
            });
        } else if (parameterType.equals(int.class) || parameterType.equals(Integer.class)) {
            processors.add((object, resultSet) -> {
                try {
                    method.invoke(object, resultSet.getInt(name));
                } catch (IllegalAccessException | InvocationTargetException error) {
                    LOGGER.warn("Set property error", error);
                }
            });
        } else if (parameterType.equals(long.class) || parameterType.equals(Long.class)) {
            processors.add((object, resultSet) -> {
                try {
                    method.invoke(object, resultSet.getLong(name));
                } catch (IllegalAccessException | InvocationTargetException error) {
                    LOGGER.warn("Set property error", error);
                }
            });
        } else if (parameterType.equals(double.class) ||  parameterType.equals(Double.class)) {
            processors.add((object, resultSet) -> {
                try {
                    method.invoke(object, resultSet.getDouble(name));
                } catch (IllegalAccessException | InvocationTargetException error) {
                    LOGGER.warn("Set property error", error);
                }
            });
        } else if (parameterType.equals(String.class)) {
            processors.add((object, resultSet) -> {
                try {
                    method.invoke(object, resultSet.getString(name));
                } catch (IllegalAccessException | InvocationTargetException error) {
                    LOGGER.warn("Set property error", error);
                }
            });
        } else if (parameterType.equals(Date.class)) {
            processors.add((object, resultSet) -> {
                try {
                    Date timestamp = resultSet.getDate(name);
                    if (timestamp != null) {
                        method.invoke(object, new Date(timestamp.getTime()));
                    }
                } catch (IllegalAccessException | InvocationTargetException error) {
                    LOGGER.warn("Set property error", error);
                }
            });
        } else {
            processors.add((object, resultSet) -> {
                String value = resultSet.getString(name);
                if (value != null && !value.isEmpty()) {
                    try {
                        method.invoke(object, Mapper.INSTANCE.getGson().fromJson(value,parameterType));
                    } catch (InvocationTargetException | IllegalAccessException error) {
                        LOGGER.warn("Set property error", error);
                    }
                }
            });
        }
    }


    public <T> Collection<T> collection(ResultSet rs, Class<T> clz) throws SQLException{
        List<T> result = new LinkedList<>();
        ResultSetMetaData resultMetaData = rs.getMetaData();
        List<ResultSetProcessor<T>> processors = new LinkedList<>();
        Method[] methods = clz.getMethods();
        for (final Method method : methods) {
            if (method.getName().startsWith("set") && method.getParameterTypes().length == 1 && !method.isAnnotationPresent(SetterIgnore.class)) {
                final String name = OperationsHelper.INSTANCE.getFieldName(method.getName().substring(3));
                boolean column = false;
                for (int i = 1; i <= resultMetaData.getColumnCount(); i++) {
                    if (name.equalsIgnoreCase(resultMetaData.getColumnLabel(i))) {
                        column = true;
                        break;
                    }
                }
                if (!column) {
                    continue;
                }
                OperationsHelper.INSTANCE.addProcessors(processors, method.getParameterTypes()[0], method, name);
            }
        }
        while (rs.next()) {
            try {
                T object = clz.newInstance();
                for (ResultSetProcessor<T> processor : processors) {
                    processor.process(object, rs);
                }
                result.add(object);
            } catch (InstantiationException | IllegalAccessException e) {
                rs.close();
                throw new SQLException(e);
            }
        }
        rs.close();
        return result;
    }

}

package libs.configFactory;

import io.vertx.core.json.JsonObject;

public enum ConfigHelper {

    INSTANCE;

    public JsonObject getMainConfig(){
        return ConfigManager.INSTANCE.getMainConfig();
    }

    public Boolean isStaging(){
        return ConfigManager.INSTANCE.getMainConfig().getString("environment").equalsIgnoreCase("staging");
    }

    public JsonObject getApplicationConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("config");
    }

    public JsonObject getMySqlConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("sql").getJsonObject("master");
    }

    public JsonObject getEventsMySqlConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("sql").getJsonObject("events");
    }

    public JsonObject getS3Config(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("s3");
    }

    public JsonObject getOlaCorpConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("apis").getJsonObject("plugins").getJsonObject("olaCorp");
    }

    public JsonObject getSlackConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("apis").getJsonObject("slack");
    }

    public JsonObject getRmqConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("rmq");
    }

    public JsonObject getFileUploadConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("fileuplaod").getJsonObject("blackhole");
    }

    public JsonObject getStaticConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("static");
    }

    public JsonObject getGmailConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("mail").getJsonObject("gmail");
    }

    public JsonObject getAwsMailConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("mail").getJsonObject("aws");
    }

    public JsonObject getCrawlingEmailConfig(){
        return ConfigManager.INSTANCE.getMainConfig().getJsonObject("mail").getJsonObject("crawlingEmail");
    }

    public String getTemplateUrl(){
        return ConfigManager.INSTANCE.getMainConfig().getString("template_path");
    }

    public Boolean isAutomation(){
        return isStaging() || ConfigManager.INSTANCE.getMainConfig().getString("environment").equalsIgnoreCase("automation");
    }
}

package libs;

import io.vertx.core.Vertx;
import io.vertx.ext.mail.MailClient;
import io.vertx.ext.mail.MailConfig;
import io.vertx.ext.mail.MailMessage;
import libs.configFactory.ConfigHelper;
import org.jtwig.JtwigTemplate;

import java.util.HashMap;
import java.util.List;

public enum MailHelper {

    INSTANCE;

    private HashMap<String, JtwigTemplate> templateHashMap = new HashMap<>();
    private MailClient sesMailClient;

    public void init(Vertx vertx) {
        if (sesMailClient == null) {
            MailConfig config = new MailConfig();
            config.setHostname(ConfigHelper.INSTANCE.getAwsMailConfig().getString("host"));
            config.setPort(ConfigHelper.INSTANCE.getAwsMailConfig().getInteger("port"));
            config.setSsl(true);
            config.setUsername(ConfigHelper.INSTANCE.getAwsMailConfig().getString("username"));
            config.setPassword(ConfigHelper.INSTANCE.getAwsMailConfig().getString("password"));
            sesMailClient = MailClient.createShared(vertx, config, "Mailer");
        }
    }

    public MailClient getMailer() {
        return sesMailClient;
    }

    public MailMessage generateEmail(String emailAddresss, String subject) {
        MailMessage message = generateAwsEmail(emailAddresss, subject);
        return message;
    }

    public MailMessage generateEmail(String emailAddresss, String subject, String service) {
        MailMessage message = generateAwsEmail(emailAddresss, subject);
        return message;
    }

    public MailMessage generateEmail(String emailAddresss, String subject, List<String> cc) {
        MailMessage message = generateAwsEmail(emailAddresss, subject, cc);
        return message;
    }

    public MailMessage generateEmail(String emailAddresss, String subject, List<String> cc, String service) {
        MailMessage message = generateAwsEmail(emailAddresss, subject, cc);
        return message;
    }

    public JtwigTemplate getTemplate(String template) {
        JtwigTemplate mFile = templateHashMap.get(template);
        if (mFile == null) {
            mFile = JtwigTemplate.fileTemplate(String.format("%s%s", ConfigHelper.INSTANCE.getTemplateUrl(), template));
            templateHashMap.put(template, mFile);
        }
        return mFile;
    }

    public MailMessage generateAwsEmail(String emailAddress, String subject) {
        MailMessage message = new MailMessage();
        message.setFrom(String.format("Eka <%s>", ConfigHelper.INSTANCE.getAwsMailConfig().getString("from")));
        message.setTo(emailAddress);
        message.setSubject(subject);
        return message;
    }

    public MailMessage generateAwsEmail(String emailAddress, String subject, List<String> ccEmails) {
        MailMessage message = new MailMessage();
        message.setFrom(String.format("Eka <%s>", ConfigHelper.INSTANCE.getAwsMailConfig().getString("from")));
        message.setTo(emailAddress);
        message.setSubject(subject);
        message.setCc(ccEmails);
        return message;
    }
}
package database.configs;

import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.NotNull;
import io.ebean.annotation.SoftDelete;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

@Data
@MappedSuperclass
public class PluginBaseModel implements MultiTenant{

    @NotNull
    @Id
    @Column(length = 40)
    private Long id;

    private String pluginId;

    @CreatedTimestamp
    private Timestamp createdAt;

    @UpdatedTimestamp
    private Timestamp updatedAt;

    @SoftDelete
    private boolean deleted;


    @Override
    public String getPluginId() {
        return pluginId;
    }
}


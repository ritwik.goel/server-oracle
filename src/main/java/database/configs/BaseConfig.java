package database.configs;

import apis.eka.Response.AuthResponse.InstallationResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.ebean.annotation.DbJsonB;
import libs.utils.Mapper;
import lombok.Data;
import lombok.NoArgsConstructor;
import plugins.core.PluginType;

import java.util.UUID;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class BaseConfig implements MultiTenant {

    private PluginType pluginType;

    private String pluginId;

    private Long installedOn = 0L;

    private Long cronRunTime = 0L;

    private String ekaClientId;

    private String ekaClientSecret;

    @DbJsonB
    private Object config;

    public void setConfig(Object o) {
        this.config = o;
    }

    public <T> T config(Class<T> t) {
        if (config == null)
            return null;
        return Mapper.INSTANCE.getGson().fromJson(Mapper.INSTANCE.getGson().toJson(config), t);
    }

    public BaseConfig(PluginType pluginType, InstallationResponse response) {
        this.pluginType = pluginType;
        this.pluginId = UUID.randomUUID().toString();
        this.ekaClientId = response.getClientId();
        this.ekaClientSecret = response.getClientSecret();
        this.installedOn = System.currentTimeMillis();
    }

    public BaseConfig(PluginType pluginType, String ekaClientId, String ekaClientSecret) {
        this.pluginType = pluginType;
        this.pluginId = UUID.randomUUID().toString();
        this.ekaClientId = ekaClientId;
        this.ekaClientSecret = ekaClientSecret;
        this.installedOn = System.currentTimeMillis();
    }
}

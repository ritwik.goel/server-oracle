package database.configs;

import database.SqlConfigFactory;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.NotNull;
import io.ebean.annotation.SoftDelete;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

@Data
@MappedSuperclass
public class BaseModel {

    @NotNull
    @Id
    @Column(length = 40)
    private Long id;

    @CreatedTimestamp
    private Timestamp createdAt;

    @UpdatedTimestamp
    private Timestamp updatedAt;

    @SoftDelete
    private boolean deleted;


    public void update(){
        SqlConfigFactory.MASTER.getServer().update(this);
    }

    public void save(){
        SqlConfigFactory.MASTER.getServer().save(this);
    }

    public void delete(){
        SqlConfigFactory.MASTER.getServer().delete(this);
    }


}


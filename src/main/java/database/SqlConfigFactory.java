package database;

import io.ebean.EbeanServer;
public enum SqlConfigFactory {

    MASTER,
    EVENTS;

    EbeanServer masterServer;
    EbeanServer eventsServer;

    public EbeanServer getServer() {
        switch (this) {
            case MASTER:
                return getMasterServer();
            case EVENTS:
                return getEventsServer();
            default:
                return getMasterServer();
        }
    }

    private EbeanServer getMasterServer() {
        if (masterServer == null) {
            synchronized (this){
                if(masterServer==null){
                }
            }
        }
        return masterServer;
    }

    public EbeanServer getEventsServer() {
        if (eventsServer == null) {
            synchronized (this){
                if (eventsServer == null) {

                }
            }
        }
        return eventsServer;
    }
}
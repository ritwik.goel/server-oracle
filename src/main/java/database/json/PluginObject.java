package database.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import plugins.core.PluginType;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PluginObject {

    private PluginType pluginType;

    private String name;

    private String description = "";

    private String logo;

    private Long installedOn = 0L;

    private String status = "ACTIVE";

    private boolean installed = false;

    private String path;

    public PluginObject(String name, String description, String logo,PluginType pluginType,String path) {
        this.name = name;
        this.description = description;
        this.logo = logo;
        this.pluginType = pluginType;
    }


}

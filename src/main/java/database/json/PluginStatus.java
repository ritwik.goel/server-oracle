package database.json;

public enum PluginStatus {

    ACTIVE,
    INACTIVE,
    ERROR
    ;

}

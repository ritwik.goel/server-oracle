package database.models;

import database.configs.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@Table(name = "upload_invoice_ocr_zip_response")
public class UploadInvoiceOcrZipResponse extends BaseModel {

    private String code;

    private String result;

    private String status;

    private String error;

    @Column(name = "total_documents")
    private Long totalDocuments = 0l;

    @Column(name = "unique_invoices")
    private Long uniqueInvoices = 0l;

    @Column(name = "duplicate_invoices")
    private Long duplicateInvoices = 0l;
}

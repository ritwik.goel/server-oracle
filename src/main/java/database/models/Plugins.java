package database.models;

import database.configs.BaseConfig;
import database.configs.BaseModel;
import database.json.PluginStatus;
import io.ebean.annotation.DbJsonB;
import io.ebean.annotation.Index;
import lombok.Data;
import lombok.EqualsAndHashCode;
import plugins.core.PluginType;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@Table(name = "plugin_users")
public class Plugins extends BaseModel {

    @Index(unique = true)
    private String code;

    private PluginType pluginType;

    private PluginStatus status;

    @DbJsonB
    private BaseConfig config;

}

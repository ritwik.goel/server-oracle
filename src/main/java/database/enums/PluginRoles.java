package database.enums;


public enum PluginRoles {

    EMPLOYEEREAD,
    EMPLOYEEWRITE,
    TRANSACTIONSCREATE,
    INVOICEUPDATE,
    TRANSACTIONSREAD,
    TRANSACTIONSUPDATE,
    ADVANCEREAD,
    VENDORREAD,
    VENDORWRITE,
    PRREAD,
    PRWRITE,
    GRNREAD,
    GRNWRITE,
    NONE;

    public String getRole(){
        switch (this){
            case EMPLOYEEREAD:
                return "employee.read";
            case EMPLOYEEWRITE:
                return "employee.write";
            case INVOICEUPDATE:
                return "invoice.update";
            case TRANSACTIONSCREATE:
                return "transaction.write";
            case TRANSACTIONSUPDATE:
                return "transaction.update" ;
            case TRANSACTIONSREAD:
                return "transaction.read" ;
            case ADVANCEREAD:
                return "advance.read" ;
            case VENDORREAD:
                return "vendor.read" ;
            case VENDORWRITE:
                return "vendor.write" ;
            case PRREAD:
                return "pr.read" ;
            case PRWRITE:
                return "pr.write" ;
            case GRNREAD:
                return "grn.read" ;
            case GRNWRITE:
                return "grn.write" ;
            default:
                return "none";
        }
    }
}
package database.repos;

import database.events.EventsFinder;
import database.events.IntegrationEvents;
import database.events.PluginSyncingEvents;
import io.ebean.ExpressionList;

public enum EventsPluginRepository {

    INSTANCE;

    private EventsFinder<Long, PluginSyncingEvents> pluginSyncingEventsFinder = new EventsFinder<>(PluginSyncingEvents.class);
    private EventsFinder<Long, IntegrationEvents> integrationEventsFinder = new EventsFinder<>(IntegrationEvents.class);

    public ExpressionList<PluginSyncingEvents> finder(String code){
        return pluginSyncingEventsFinder.query().where().eq("company_code", code);
    }

    public ExpressionList<IntegrationEvents> integrationEventFinder(String code){
        return integrationEventsFinder.query().where().eq("company_code", code);
    }
}
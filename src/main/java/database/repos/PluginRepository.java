package database.repos;

import database.MasterFinder;
import database.configs.BaseConfig;
import database.models.Plugins;
import plugins.core.PluginType;

import java.util.List;

public enum PluginRepository {

    INSTANCE;

    private MasterFinder<Long, Plugins> finder = new MasterFinder<>(Plugins.class);


    public List<Plugins> pluginsForCode(String code) {
        return finder.query().where().eq("code", code).findList();
    }


    public Plugins findPlugin(String code, PluginType type) {
        return finder.query().where().eq("code", code).eq("plugin_type", type).findOne();
    }

    public List<Plugins> findAll(PluginType pluginType) {
        return finder.query().where().eq("plugin_type", pluginType).findList();
    }

    public Plugins pluginForUpdate(Plugins plugins) {
        plugins = finder.query().where()
                .eq("code", plugins.getCode())
                .eq("plugin_type", plugins.getPluginType())
                .findOne();
        if (plugins == null)
            return null;
        BaseConfig baseConfig = plugins.getConfig();
        Long lastSynced = baseConfig.getCronRunTime();
        Long current = System.currentTimeMillis();
        lastSynced = lastSynced + 30 * 60 * 1000;
        if (current < lastSynced) {
            return null;
        }
        baseConfig.setCronRunTime(System.currentTimeMillis());
        plugins.setConfig(baseConfig);
        plugins.update();
        return plugins;
    }

    public Plugins pluginForUpdate(Plugins plugins, int minGap) {
        plugins = finder.query().where()
                .eq("code", plugins.getCode())
                .eq("plugin_type", plugins.getPluginType())
                .forUpdateSkipLocked()
                .findOne();
        if (plugins == null)
            return null;
        BaseConfig baseConfig = plugins.getConfig();
        Long lastSynced = baseConfig.getCronRunTime();
        Long current = System.currentTimeMillis();
        lastSynced = lastSynced + minGap * 60 * 1000;
        if (current < lastSynced) {
            return null;
        }
        baseConfig.setCronRunTime(System.currentTimeMillis());
        plugins.setConfig(baseConfig);
        plugins.update();
        return plugins;
    }


}

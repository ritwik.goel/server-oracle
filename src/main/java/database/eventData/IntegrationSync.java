package database.eventData;

import database.SqlConfigFactory;
import database.events.IntegrationEvents;
import database.repos.EventsPluginRepository;
import io.ebean.Transaction;
import libs.utils.SlackNotifier;
import plugins.core.PluginType;

import java.util.regex.Pattern;

public enum IntegrationSync {

    INSTANCE;

    private int minTimeGap = 5;
    {
        minTimeGap = 5;
    }

    public void runIntegrationSync(String time){
        try {
            if (Long.parseLong(time.split(Pattern.quote(":"))[1]) % minTimeGap != 0L){
                return;
            }
            syncEvents();
        }catch (Exception e){
            e.printStackTrace();
            SlackNotifier.INSTANCE.hitSlackCommon(e.getMessage());
        }
        System.out.println("time ::: "+time);
    }

    private void syncEvents(){
        try (Transaction transaction = SqlConfigFactory.EVENTS.getServer().beginTransaction()) {

            for (PluginType value : PluginType.values()) {
                String companyCode = value.getPlugin().companyCode();
                if (companyCode != null){
                    for (IntegrationEvents integrationEvents : EventsPluginRepository.INSTANCE.integrationEventFinder(companyCode)
                            .eq("is_synced", false).findList()) {
                        value.getPlugin().syncIntegration(integrationEvents, value.getPlugin());
                    }
                }
            }

            transaction.commit();
            transaction.end();
        }catch (Exception e){
            e.printStackTrace();
            SlackNotifier.INSTANCE.hitSlackCommon(e.getMessage());
        }
    }
}
package database.eventData;

public interface IntegrationEventTypes {

    final String CreateVendor = "vendor.create";

    final String AddVendorAddress = "vendor.address.add";

    final String CreateInvoice = "invoice.create";

    final String InvoicePaid = "invoice.paid";

    final String CreateVoucher = "voucher.create";

    final String VoucherPaid = "voucher.paid";

    final String CreateAdvanceRequest = "advance.request.create";

    final String AdvanceRequestPaid = "advance.request.paid";

    final String CreateGrn = "grn.create";

    final String CreatePo = "po.create";

    final String CreateEmployee = "employee.create";

    final String UpdateEmployee = "employee.update";

}
package database.eventData.integration.Invoice;

import database.eventData.integration.common.*;
import database.eventData.integration.enums.PurchaseType;
import database.eventData.integration.enums.Status;
import lombok.Data;

import static database.eventData.integration.enums.Status.*;

@Data
public class PurchaseOrder {

    private AdminUser adminUser;

    private String name;

    private Status status = Status.PENDING;

    private String currency = "INR";

    private Double currencyAmount = 0D;

    public Double getCurrencyAmount() {
        if(currencyAmount==null || currencyAmount==0D )
            currencyAmount = amount;
        return currencyAmount;
    }

    private Double amount = 0D;

    private Double invoicedAmount = 0D;

    private Employee grn;

    private PurchaseType purchaseType = PurchaseType.REQUEST;

    private String orderId;

    private String vendorConditions;

    private Vendor vendor;

    private Address vendorAddress;

    private PurchaseOrderData content = new PurchaseOrderData();

    private String category;

    private OfficeDetails office;

    public boolean pending(){
        return status.equals(PENDING)
                || status.equals(APPROVAL)
                || status.equals(TRIGGERED)
                || status.equals(DRAFT)
                || status.equals(SCANNER)
                || status.equals(QUOTATIONS)
                || status.equals(INVOICE)
                || status.equals(PUSHED);
    }

    public boolean completed() {
        return status.equals(Status.COMPLETED) || status.equals(Status.FAILED) || status.equals(Status.CANCELLED)
                || status.equals(Status.DECLINED) ;
    }
}
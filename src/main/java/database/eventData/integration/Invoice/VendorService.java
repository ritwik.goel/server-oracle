package database.eventData.integration.Invoice;

import lombok.Data;

import java.util.HashMap;

@Data
public class VendorService {

    private String category;

    private String name;

    private String expenseType = "CAPEX";

    private String type = "invoicing";

    private String goodType;

    private HashMap<String,String> attrs = new HashMap<>();
}
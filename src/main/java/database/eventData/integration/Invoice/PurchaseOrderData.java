package database.eventData.integration.Invoice;

import database.eventData.integration.common.Comment;
import database.eventData.integration.common.Payment;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PurchaseOrderData {


    private List<String> files = new ArrayList<>();

    private List<Comment> comments = new ArrayList<>();

    private List<Payment> payments = new ArrayList<>();

    private boolean invoiceApproval = true;

    private Long recurringId = 0L;
}

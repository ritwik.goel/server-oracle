package database.eventData.integration.Invoice;

import database.eventData.integration.common.*;
import database.eventData.integration.enums.ReconStatus;
import database.eventData.integration.enums.Status;

import java.sql.Timestamp;

import static database.eventData.integration.enums.Status.PENDING;

public class VendorInvoice {

    private Employee employeeUser;

    private Vendor vendorUser;

    private AdminUser adminUser;

    private String name;

    private String invoiceId;

    private Long date;

    private Double amount = 0D;

    private String currency = "INR";

    private Double currencyAmount = 0D;

    private Status status = PENDING;

    public Double getCurrencyAmount() {
        if(currencyAmount==null || currencyAmount==0D )
            currencyAmount = amount;
        return currencyAmount;
    }

    private String category;

    private VendorService service;

    private Settlement settlement;

    private PurchaseOrder po;

    private String type;

    private Timestamp dueDate;

    private OfficeDetails office;

    private OfficeDetails deliveryAddress;

    private Vendor vendor;

    private Address vendorAddress;

    private ReconStatus reconStatus = ReconStatus.SKIPPED;

    private VendorInvoiceConfig config = new VendorInvoiceConfig();

    private Object taxInfo;

    private boolean grn = false;

    public boolean completed() {
        return status.equals(Status.COMPLETED) || status.equals(Status.FAILED) || status.equals(Status.CANCELLED)
                || status.equals(Status.DECLINED) ;
    }
}
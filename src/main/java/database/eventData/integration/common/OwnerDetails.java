package database.eventData.integration.common;

import database.eventData.integration.enums.MetaType;
import lombok.Data;

@Data
public class OwnerDetails {
    private String ownerType = MetaType.EMPLOYEE.getValue();
    private String ownerId;
    private String ownerName;
}

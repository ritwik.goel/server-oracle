package database.eventData.integration.common;

import lombok.Data;

import java.util.HashMap;

@Data
public class Employee {

    private String email;

    private String mobile;

    private String name;

    private String code;

    private String team;

    private String department;

    private OfficeDetails office;

    private HashMap<String, String> customFields = new HashMap<>();
}
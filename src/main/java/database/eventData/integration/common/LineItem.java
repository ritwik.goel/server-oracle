package database.eventData.integration.common;

import lombok.Data;

@Data
public class LineItem {

    private Long productId = 0L;

    private String name;

    private Double price;

    private String hsn;

    private Long qtty = 0L;

    private Long grnQty = 0L;

    private String service;

    private String currency;

    private Double taxSlab = 0D;

    private Long referenceId = 0L;

    private Double gst = 0D;

    public Double getTaxSlab() {
        if(taxSlab==null || taxSlab.isNaN() || taxSlab == 0)
            taxSlab = gst;
        return taxSlab;
    }

    private Double tax = 0D;

    private Double base;

    private Double amount = 0D;

    public Long getGrnQty() {
        if (grnQty == null){
            grnQty = 0l;
        }
        return grnQty;
    }
}
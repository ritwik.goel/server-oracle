package database.eventData.integration.common;

import lombok.Data;

@Data
public class OfficeDetails {

    private String name;

    private String gstin;

    private String registeredName;

    private String registeredAddress;

    private String currency = "INR";
}

package database.eventData.integration.common;

import lombok.Data;

@Data
public class Comment {

    private String message;

    private Long time;

    private OwnerDetails owner;

}

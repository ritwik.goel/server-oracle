package database.eventData.integration.common;

import lombok.Data;

@Data
public class Integration {

    private boolean completed = false;

    private String type;

    private String logo;

    private Long createdAt;

    private String id;

    private String message;

    private String status;

}

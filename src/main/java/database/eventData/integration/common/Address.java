package database.eventData.integration.common;

import lombok.Data;

import java.util.UUID;

@Data
public class Address {

    private String id = UUID.randomUUID().toString();

    private String title;

    private String address ;

    private String city;

    private String state;

    private String country;

    private String pincode;

    private String gstin;

    private String locationId;

    private String filterKey;

    public String getLocationId() {
        if (locationId == null){
            locationId = "";
        }
        return locationId;
    }

    public String getFilterKey() {
        if (filterKey == null){
            filterKey = "";
        }
        return filterKey;
    }

    public String getId(){
        if(id==null)
            id = "default";
        return id;
    }
}
package database.eventData.integration.common;

import lombok.Data;

@Data
public class Payment {

    private Long id;

    private String title;

    private Double amount;

    private Long time;

}

package database.eventData.integration.common;

import database.eventData.integration.enums.SettlementDataType;
import lombok.Data;
import rmq.data.Enums.MetaType;

@Data
public class Settlement {

    private String ledgerId;

    private Long ownerId;

    private MetaType ownerType = MetaType.EMPLOYEE;

    private SettlementDataType referenceType = SettlementDataType.REIMBURSEMENT;

    private String currency = "INR";

    private Double partial = 0D;

    private Double partialTds = 0D;

    private Double amount = 0D;

    //Paid amount to their bank account minus TDS
    private Double paidAmount = 0D;

    private Double tdsAmount = 0D;

    private boolean tdsSettled = true;

    private Double reverseCharge = 0D;

    private boolean reverseSettled = true;

    //To track if payout is completed or not
    private boolean completed = true;

    //Track if settlement is settled
    private boolean settled = false;

    public boolean isDebit(){
        boolean debit = false;
        switch (referenceType){
            case RECALLVOUCHER:
            case RECALLADVANCE:
            case PAYOUTS:
            case PENALTY:
            case PARTIAL:
            case RECEIVABLEREFUND:
            case INVOICERAISED:
            case POADVANCE:
            case VENDORADVANCE:
            case SETTLED:
                debit = true;
                break;
            case RECEIVED:
            case REIMBURSEMENT:
            case PETTYCASH:
            case INVOICES:
            case ADVANCE:
            case ADDADVANCE:
            case INVOICETDS:
            case RECEIVABLE:
            case RECALLPAYOUTS:
            default:
                debit = false;
        }
        return debit;
    }
    public Double settlement(){
        if(isDebit())
            return -1 * amount;
        return amount;
    }

    public Double getPendingAmount(){
        return getAmount() - getTotalPartialPayment();
    }

    public Double actualPayable(){
        return  getPaidAmount() - partial;
    }

    public Double actualTdsPayable(){
        return  tdsAmount - partialTds;
    }

    private Double getTotalPartialPayment(){
        return partial + partialTds;
    }

    public Double getPaidAmount() {
        if(paidAmount==0D)
            paidAmount = amount;
        return paidAmount;
    }
}
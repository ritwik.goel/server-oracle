package database.eventData.integration.common;

import lombok.Data;

@Data
public class AdminUser {
    private String email;

    private String name;
}
package database.eventData.integration.common;


import io.vertx.core.json.JsonObject;
import lombok.Data;

import java.util.HashMap;

@Data
public class UtilityInvoice {

    private Long linkedId = 0L;

    private JsonObject tags;

    private Double amount;

    private String service;

    private String date;

    private String dueDate;

    private String billNumber;

    private HashMap<String,String> attrs = new HashMap<>();

}

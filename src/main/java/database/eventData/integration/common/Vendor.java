package database.eventData.integration.common;

import lombok.Data;

@Data
public class Vendor {

    private String name;

    private String email = "";

    private String mobile = "";

    private String pan;

    private String gstin;

    private String currency = "INR";

    private String registeredId;

    private Address address;

}
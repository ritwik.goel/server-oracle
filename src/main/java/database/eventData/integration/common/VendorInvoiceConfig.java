package database.eventData.integration.common;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class VendorInvoiceConfig {

    List<String> files = new ArrayList<>();

    List<Comment> comments = new ArrayList<>();

    UtilityInvoice utility;

    private Integration integration;

    public Integration getIntegration() {
        if(integration==null)
            integration = new Integration();
        return integration;
    }

    List<LineItem> items = new ArrayList<>();
}
package database.eventData.integration.Advance;

import database.eventData.integration.common.Settlement;
import database.eventData.integration.enums.Status;
import lombok.Data;

@Data
public class AdvanceRequest {

    private AdvanceDetails advance;

    private Double amount;

    private Status status = Status.PENDING;

    private String title;

    private String description;

    private String remarks;

    private String type;

    private Settlement settlement;
}

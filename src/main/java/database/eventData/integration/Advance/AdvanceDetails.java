package database.eventData.integration.Advance;

import database.eventData.integration.common.Employee;
import lombok.Data;

@Data
public class AdvanceDetails {

    private Employee employee;

    private String title;

    private String description;

    private Double balanceLeft = 0D;

    private Double transferredAmount = 0D;

    private Double initialAmount = 0D;

    private String type;

    private boolean completed = false;

    private boolean settled = false;

}

package database.eventData.integration.enums;

public enum VoucherStatus {

    CREATED("CREATED"),
    APPROVAL("APPROVAL"),
    FINANCE("FINANCE"),
    DECLINED("DECLINED"),
    CANCELLED("CANCELLED"),
    REIMBURSED("REIMBURSED");

    String dbValue;

    VoucherStatus(String dbValue) {
        this.dbValue = dbValue;
    }

    public String getValue() {
        return dbValue;
    }

}
package database.eventData.integration.enums;

public enum TransactionType {

    //Expenses
    SELFPAID("SELFPAID"),
    WALLETPAID("WALLETPAID"),
    PETTYPAID("PETTYPAID"),

    //Qr Payments
    COMPANYPAID("COMPANYPAID"),
    QRPAID("QRPAID"),

    //Vendor Invoices
    INVOICES("INVOICES"),
    UTILITY("UTILITY"),


    TRANSFER("TRANSFER"),
    RECHARGE("RECHARGE"),
    COLLECTION("COLLECTION"),
    REFUND("REFUND");

    String dbValue;
    TransactionType(String dbValue) {
        this.dbValue = dbValue;
    }

    public String getValue() {
        return dbValue;
    }
}

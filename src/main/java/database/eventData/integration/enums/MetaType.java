package database.eventData.integration.enums;

public enum MetaType {

    SYSTEM("SYSTEM"),
    SME("SME"),
    VENDOR("VENDOR"),
    EMPLOYEE("EMPLOYEE"),
    PERSONAL("PERSONAL"),
    CONTACT("CONTACT"),
    PAYOUT("PAYOUT"),
    TDESK("TDESK"),
    GUEST("GUEST"),
    SCANNER("SCANNER"),
    ADMIN("ADMIN"),
    SUPER("SUPER"),
    SUPPORT("SUPPORT"),
    COMPANY("COMPANY");

    String dbValue;

    MetaType(String dbValue) {
        this.dbValue = dbValue;
    }

    public String getValue() {
        return dbValue;
    }
}

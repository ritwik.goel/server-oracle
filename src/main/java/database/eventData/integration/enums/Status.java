package database.eventData.integration.enums;

public enum Status {

    CREATED("CREATED"),
    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE"),
    SUGGEST("SUGGEST"),
    INVOICE("INVOICE"),
    ADDITION("ADDITION"),
    SCANNER("SCANNER"),
    PAYMENT("PAYMENT"),
    POHOLD("POHOLD"),
    TRIGGERPAYMENT("TRIGGERPAYMENT"),
    QUOTATIONS("QUOTATIONS"),
    GRN("GRN"),
    TRIGGERED("TRIGGERED"),
    APPROVAL("APPROVAL"),
    ADMINTOUCH("ADMINTOUCH"),
    PENDING("PENDING"),
    SCHEDULED("SCHEDULED"),
    PUSHED("PUSHED"),
    DECLINED("DECLINED"),
    FAILED("FAILED"),
    DRAFT("DRAFT"),
    CANCELLED("CANCELLED"),
    SKIPPED("SKIPPED"),
    REVERTED("REVERTED"),
    COMPLETED("COMPLETED");

    String dbValue;

    Status(String dbValue) {
        this.dbValue = dbValue;
    }

    public String getValue() {
        return dbValue;
    }

}

package database.eventData.integration.enums;

public enum ReconStatus {

    SKIPPED("SKIPPED"),
    PENDING("PENDING"),
    FAILED("FAILED"),
    CANCELLED("CANCELLED"),
    SUCCESS("SUCCESS"),
    RECONFAILED("RECONFAILED")
    ;

    String dbValue;

    ReconStatus(String dbValue) {
        this.dbValue = dbValue;
    }

    public String getValue() {
        return dbValue;
    }
}

package database.eventData.integration.enums;

public enum PaymentMethod {

    /*
        * This are payment method type which can be reimbursed
    */
    SELFOFFLINE("SELFOFFLINE"),
    SELFONLINE("SELFONLINE"),
    WALLET("WALLET"),
    ACCOUNTS("ACCOUNTS"),
    IMPORT("IMPORT"),
    MIS("MIS"),
    PAYOUTS("PAYOUTS"),
    INVOICE("INVOICE"),
    PETTYCASH("PETTYCASH");

    String dbValue;
    PaymentMethod(String dbValue) {
        this.dbValue = dbValue;
    }

    public String getValue() {
        return dbValue;
    }
}

package database.eventData.integration.enums;

public enum PurchaseType {

    REQUEST("REQUEST"),
    CONTRACT("CONTRACT"),
    CREATE("CREATE"),
    ;

    String dbValue;

    PurchaseType(String dbValue) {
        this.dbValue = dbValue;
    }

    public String getValue() {
        return dbValue;
    }
}

package database.eventData.integration.Voucher;

import database.eventData.integration.common.OwnerDetails;
import database.eventData.integration.common.RepositoryItem;
import database.eventData.integration.enums.MetaType;
import database.eventData.integration.enums.PaymentMethod;
import database.eventData.integration.enums.TransactionType;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;

@Data
public class TransactionDetails {

    private String service;

    private HashMap<String, String> serviceAttrs;

    private MetaType ownerType;

    private OwnerDetails owner;

    private String pluginId;

    private TransactionType transactionType = TransactionType.SELFPAID;

    private PaymentMethod paymentMethodType;

    private Long date;

    private String currency = "INR";

    private Double amount = 0D;

    private Double reimbursableAmount = 0D;

    private Double balance = 0D;

    private String officeName;

    private String departmentName;

    private String title;

    private String merchantName;

    private String details;

    private String tripId;

    private String currencyValue = "INR";

    private Double currencyAmount;

    private Long refundId = 0L;

    private Long refundAmount = 0L;

    private String ticket;

    private boolean refunded = false;

    private Long expenseStatus = 1L;

    private HashMap<String, String> allFields = new HashMap<>();

    private ArrayList<RepositoryItem> files = new ArrayList<>();
}
package database.eventData.integration.Voucher;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class VoucherRequest {

    private VoucherDetails voucher;

    private List<TransactionDetails> transactions = new ArrayList<>();
}

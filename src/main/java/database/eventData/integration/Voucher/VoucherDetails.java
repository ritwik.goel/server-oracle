package database.eventData.integration.Voucher;

import database.eventData.integration.common.Employee;
import database.eventData.integration.common.Settlement;
import database.eventData.integration.enums.VoucherStatus;
import lombok.Data;

@Data
public class VoucherDetails {

    private String voucherId;

    private Employee employee;

    private VoucherStatus status = VoucherStatus.CREATED;

    private String currency = "INR";

    private Double initialAmount = 0D;

    private Double totalAmount = 0D;

    private Long approvedAt;

    private Double claimedAmount = 0D;

    private Double pettyAmount = 0D;

    private Settlement reimbursement;

    private Settlement settlement;

}
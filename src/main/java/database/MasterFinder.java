package database;

import io.ebean.EbeanServer;
import io.ebean.Query;
import io.ebean.Transaction;
import io.ebean.UpdateQuery;

import java.util.List;

public class MasterFinder<I, T> {

  private final Class<T> type;

  private final String serverName;

  public MasterFinder(Class<T> type) {
    this.type = type;
    this.serverName = null;
  }


  public Transaction currentTransaction() {
    return db().currentTransaction();
  }

  public void flush() {
    db().flush();
  }


  public EbeanServer db() {
    return SqlConfigFactory.MASTER.getServer();
  }

  public T ref(I id) {
    return db().getReference(type, id);
  }

  public T byId(I id) {
    return db().find(type, id);
  }

  public void deleteById(I id) {
    db().delete(type, id);
  }

  public List<T> all() {
    return query().findList();
  }

  public UpdateQuery<T> update() {
    return db().update(type);
  }

  public Query<T> query() {
    return db().find(type);
  }

  public Query<T> nativeSql(String nativeSql) {
    return db().findNative(type, nativeSql);
  }


  public Query<T> query(String ormQuery) {
    return db().createQuery(type, ormQuery);
  }

}
package database.events;

import database.SqlConfigFactory;
import io.ebean.EbeanServer;
import io.ebean.Query;
import io.ebean.Transaction;
import io.ebean.UpdateQuery;

import java.util.List;

public class EventsFinder<I, T> {

  private final Class<T> type;


  public EventsFinder(Class<T> type) {
    this.type = type;
  }

  public Transaction currentTransaction() {
    return db().currentTransaction();
  }

  public void flush() {
    db().flush();
  }

  private EbeanServer db() {
    return SqlConfigFactory.EVENTS.getServer();
  }

  public T ref(I id) {
    return db().getReference(type, id);
  }

  public T byId(I id) {
    return db().find(type, id);
  }

  public void deleteById(I id) {
    db().delete(type, id);
  }

  public List<T> all() {
    return query().findList();
  }

  public UpdateQuery<T> update() {
    return db().update(type);
  }

  public Query<T> query() {
    return db().find(type);
  }

  public Query<T> nativeSql(String nativeSql) {
    return db().findNative(type, nativeSql);
  }


  public Query<T> query(String ormQuery) {
    return db().createQuery(type, ormQuery);
  }

}
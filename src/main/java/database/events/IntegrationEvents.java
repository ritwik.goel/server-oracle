package database.events;

import database.SqlConfigFactory;
import database.eventData.IntegrationEventTypes;
import io.ebean.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@Table(name = "integration_events")
public class IntegrationEvents {

    @NotNull
    @Id
    @Column(length = 40)
    private Long id;

    @CreatedTimestamp
    private Timestamp createdAt;

    @UpdatedTimestamp
    private Timestamp updatedAt;

    @SoftDelete
    private boolean deleted;

    @Index
    @NotNull
    @Column(length = 40)
    private Long companyId;

    @Column(name = "company_code")
    private String companyCode;

    private String type = IntegrationEventTypes.CreateInvoice;

    @Column(name = "eka_id")
    private String ekaId;

    @Column(name = "is_synced")
    private Boolean isSynced = false;

    @DbJsonB
    private String request;

    public void save() {
        SqlConfigFactory.EVENTS.getServer().save(this);
    }

    public void update() {
        SqlConfigFactory.EVENTS.getServer().update(this);
    }

    public void delete() {
        SqlConfigFactory.EVENTS.getServer().delete(this);
    }
}

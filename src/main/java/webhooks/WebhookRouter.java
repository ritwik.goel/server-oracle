package webhooks;

import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.Router;
import plugins.custom.cjdarcl.sap.webhook.NewBranchDetailsController;
import plugins.custom.cjdarcl.sap.webhook.NewExpenseTypeController;
import plugins.custom.cjdarcl.sap.webhook.NewTaxCodeController;
import plugins.custom.cjdarcl.sap.webhook.NewVendorMasterController;

public enum WebhookRouter {

    INSTANCE;

    public Router router(Vertx vertx){
        Router router = Router.router(vertx);
        router.post("/newBranchCode").handler(NewBranchDetailsController.INSTANCE::handle);
        router.post("/newExpenseType").handler(NewExpenseTypeController.INSTANCE::handle);
        router.post("/newTaxCode").handler(NewTaxCodeController.INSTANCE::handle);
        router.post("/newVendor").handler(NewVendorMasterController.INSTANCE::handle);

        return router;
    }
}

package oauth.design;

import com.auth0.jwt.algorithms.Algorithm;
import oauth.TokenGenerator;

import java.io.UnsupportedEncodingException;

public class Oauth {

    private static Algorithm algorithm=null;

    public static Algorithm Algo() throws UnsupportedEncodingException {
        if(algorithm==null){
            algorithm=new TokenGenerator().algo();
        }
        return algorithm;
    }
}

package oauth.helper;

import libs.configFactory.ConfigHelper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

@SuppressWarnings("deprecation")
public class PublicKeyReader {

    public static PublicKey get() {
        try {
            byte[] keyBytes = Files.readAllBytes(Paths.get(ConfigHelper.INSTANCE.getApplicationConfig().getString("public_key")));
            X509EncodedKeySpec spec =
                    new X509EncodedKeySpec(keyBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePublic(spec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

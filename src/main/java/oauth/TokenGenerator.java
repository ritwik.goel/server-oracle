package oauth;

import com.auth0.jwt.algorithms.Algorithm;
import oauth.helper.PrivateKeyReader;
import oauth.helper.PublicKeyReader;

import java.io.UnsupportedEncodingException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

public class TokenGenerator {

    private Algorithm algorithm;

    public TokenGenerator() throws UnsupportedEncodingException {
        RSAPrivateKey pvtkey=(RSAPrivateKey) PrivateKeyReader.get();
        RSAPublicKey publicKey=(RSAPublicKey) PublicKeyReader.get();
        algorithm= Algorithm.RSA256(publicKey,pvtkey);
    }

    public Algorithm algo() {
        return algorithm;
    }
}

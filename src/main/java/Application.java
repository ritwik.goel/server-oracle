import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.VertxOptions;
import io.vertx.oracleclient.OracleConnectOptions;
import io.vertx.oracleclient.OraclePool;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import libs.MailHelper;
import libs.TemplateLoader;
import libs.configFactory.ConfigManager;
import rmq.RMQRouter;
import verticles.Crons;
import verticles.HttpRouter;

import java.util.concurrent.CompletableFuture;


public class Application extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        super.start();
        VertxOptions vertxOptions = new VertxOptions();
        int eventLoopSize = vertxOptions.getEventLoopPoolSize();
        ConfigManager.INSTANCE.setMainConfig(config());
        vertx.deployVerticle(HttpRouter.class.getName(),new DeploymentOptions().setWorkerPoolSize(40).setInstances(eventLoopSize));
        CompletableFuture.supplyAsync(()->{
            try {
                TemplateLoader.INSTANCE.init(vertx);
                Crons.INSTANCE.handle(vertx);
                MailHelper.INSTANCE.init(vertx);
//                SqlConfigFactory.MASTER.getServer();
//                SqlConfigFactory.EVENTS.getServer();
                RMQRouter.INSTANCE.createRMQClient(vertx);
                System.out.println("Max Memory ::: "+Runtime.getRuntime().maxMemory());
                System.out.println("Free Memory on start ::: "+Runtime.getRuntime().freeMemory());


                //database connection:
                OracleConnectOptions connectOptions = new OracleConnectOptions()
                        .setPort(1521)
                        .setHost("the-host")

                        .setDatabase("the-db")
                        .setUser("user")
                        .setPassword("secret");

// Pool options
                PoolOptions poolOptions = new PoolOptions()
                        .setMaxSize(5);

// Create the client pool
                OraclePool client = OraclePool.pool(connectOptions, poolOptions);

// A simple query
                client
                        .query("SELECT * FROM users WHERE id='julien'")
                        .execute(ar -> {
                            if (ar.succeeded()) {
                                RowSet<Row> result = ar.result();
                                System.out.println("Got " + result.size() + " rows ");
                            } else {
                                System.out.println("Failure: " + ar.cause().getMessage());
                            }

                            // Now close the pool
                            client.close();
                        });



            }catch (Exception e){
                e.printStackTrace();
            }
           return null;
        });

    }
}
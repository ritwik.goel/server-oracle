FROM gradle:jdk8 as builder

COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN ./gradlew clean shadowJar --stacktrace

FROM openjdk:8-jre-slim
EXPOSE 9001
COPY --from=builder /home/gradle/src/build/libs/plugin-server.jar  /app/
COPY --from=builder /home/gradle/src/build/resources/  /app/
WORKDIR /app
RUN ls -lah
ARG DOCKER_ENV

CMD java -jar plugin-server.jar run Application -conf main/${DOCKER_ENV}/config.json